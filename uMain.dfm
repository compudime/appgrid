object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'AppGrid 0.5'
  ClientHeight = 364
  ClientWidth = 578
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object gridPanel: TGridPanel
    Left = 0
    Top = 0
    Width = 578
    Height = 334
    Align = alClient
    BevelOuter = bvNone
    ColumnCollection = <
      item
        Value = 50.000286098475080000
      end
      item
        Value = 49.999713901524920000
      end>
    ControlCollection = <>
    RowCollection = <
      item
        Value = 49.998535141944750000
      end
      item
        Value = 50.001464858055260000
      end>
    TabOrder = 0
    OnResize = gridPanelResize
  end
  object FlowPanelGeneral: TPanel
    Left = 0
    Top = 334
    Width = 578
    Height = 30
    Align = alBottom
    TabOrder = 1
  end
  object tmrInitialize: TTimer
    Enabled = False
    Interval = 200
    OnTimer = tmrInitializeTimer
    Left = 72
    Top = 8
  end
  object ActionList: TActionList
    Left = 312
    Top = 40
    object acOpenSettings: TAction
      Caption = 'Open Settings'
      ShortCut = 16467
      OnExecute = acOpenSettingsExecute
    end
  end
  object MQTTClient: TTMSMQTTClient
    Version = '1.3.0.2'
    Left = 144
    Top = 120
  end
  object TimerMQTT: TTimer
    Enabled = False
    OnTimer = TimerMQTTTimer
    Left = 352
    Top = 216
  end
end
