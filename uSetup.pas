unit uSetup;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, System.IniFiles, uConst, System.StrUtils,
  Vcl.Samples.Spin, Vcl.Menus, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, cxButtons;

type
  TfrmSetup = class(TForm)
    gridPanel: TGridPanel;
    pnlGral: TPanel;
    StaticText1: TStaticText;
    edPass: TLabeledEdit;
    edGridWidth: TLabeledEdit;
    edGridHeight: TLabeledEdit;
    edMarginTop: TLabeledEdit;
    edMarginBottom: TLabeledEdit;
    edMarginLeft: TLabeledEdit;
    edMarginRight: TLabeledEdit;
    ckAutoSize: TCheckBox;
    StaticText2: TStaticText;
    btnSave: TButton;
    btnReload: TButton;
    Button2: TButton;
    OpenDialog: TOpenDialog;
    pnlPass: TPanel;
    edLogin: TLabeledEdit;
    SpinEditIdle: TSpinEdit;
    Label2: TLabel;
    Label1: TLabel;
    SpinEditGeneralSize: TSpinEdit;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    ComboBoxGeneralPlacement: TComboBox;
    CheckBoxGeneralShow: TCheckBox;
    TimerShowProperty: TTimer;
    FlowPanelGeneral: TPanel;
    ButtonAddGeneralActionButton: TButton;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    EditMQTTServer: TEdit;
    Label5: TLabel;
    EditMQTTClientID: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure edLoginKeyPress(Sender: TObject; var Key: Char);
    procedure Button2Click(Sender: TObject);
    procedure btnReloadClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure TimerShowPropertyTimer(Sender: TObject);
    procedure CheckBoxGeneralShowClick(Sender: TObject);
    procedure ComboBoxGeneralPlacementChange(Sender: TObject);
    procedure SpinEditGeneralSizeChange(Sender: TObject);
    procedure ButtonAddGeneralActionButtonClick(Sender: TObject);
  private
    { Private declarations }
    FgHeight: Integer;
    FgWidth: Integer;
    FIniFile: TIniFile;
    procedure GenerateGrid;
    procedure LoadProgramsInfo;
    procedure SetTitlePanel(Col, Row: Integer; Title: String; Visible: Boolean; AFont: TFont);
    function  FindAppPanel(Col, Row: Integer): TPanel;
    procedure btnPathClick(Sender: TObject);
    procedure btnChangeFontClick(Sender: TObject);
    function  checkPass: Boolean;
    function  Encrip_Decrip(Pass: String): String;
    procedure SaveSettings;
    procedure btnAddClick(Sender: TObject);
    procedure btnPropertyClick(Sender: TObject);
    procedure ActionBarSizeChange(Sender: TObject);
    procedure ActionBarVisibleChange(Sender: TObject);
//    procedure ActionBarButtonsChange(Sender: TObject);
    procedure ActionBarPlacementChange(Sender: Tobject);
    procedure RealignPanelButtons(APanel : TPanel);
    procedure AlignButton(var AButton: TMyButton);
  public
    { Public declarations }
  end;

var
  frmSetup: TfrmSetup;

implementation

{$R *.dfm}

uses uProperty;

procedure TfrmSetup.ActionBarPlacementChange(Sender: Tobject);
var
  LPanel : TPanel;
  LPrevAlign : TAlign;
  LIndex : integer;
begin
  LPanel := TPanel(self.FindComponent(StringReplace(TComboBox(Sender).Name,'cbActionBarAlign','actionBar',[])));
  LPrevAlign := LPanel.Align;
  if (((LPrevAlign = TAlign.alLeft) or (LPrevAlign = TAlign.alRight))
    and (TComboBox(Sender).ItemIndex = 1)) or
    (((TComboBox(Sender).ItemIndex = 0) or (TComboBox(Sender).ItemIndex = 2))
    and (LPrevAlign = TAlign.alBottom)) then
   for LIndex := 0 to LPanel.ControlCount - 1 do
    TMyButton(LPanel.Controls[LIndex]).Align := TAlign.alNone;

  case TComboBox(Sender).ItemIndex of
    0: LPanel.Align := TAlign.alLeft;
    1: LPanel.Align := TAlign.alBottom;
    2: LPanel.Align := TAlign.alRight;
  end;
  if (LPrevAlign <> LPanel.Align) then
  begin
    if LPrevAlign = TAlign.alBottom then
    begin
      RealignPanelButtons(LPanel);
    end
    else if LPanel.Align = TAlign.alBottom then
    begin
      RealignPanelButtons(LPanel);
    end;
  end;

end;

procedure TfrmSetup.ActionBarSizeChange(Sender: TObject);
var
  LFlowPanel : TPanel;
  LComboBox : TComboBox;
begin
  if TSpinEdit(Sender).Text <> '' then
  begin
    LFlowPanel := TPanel(self.FindComponent(StringReplace(TSpinEdit(Sender).Name,'seActionBarSize','actionBar',[])));
    LComboBox := TComboBox(self.FindComponent(StringReplace(TButton(Sender).Name,'seActionBarSize','cbActionBarAlign',[])));
    if LComboBox.ItemIndex = 1 then
      LFlowPanel.Height := Scale(TSpinEdit(Sender).Value)
    else
      LFlowPanel.Width := Scale(TSpinEdit(Sender).Value)
  end;
end;

procedure TfrmSetup.ActionBarVisibleChange(Sender: TObject);
var
  LPanel : TPanel;
begin
  LPanel := TPanel(self.FindComponent(StringReplace(TCheckBox(Sender).Name,'ckShowActionBar','actionBar',[])));
  LPanel.Visible := TCheckBox(Sender).Checked;
end;

procedure TfrmSetup.AlignButton(var AButton: TMyButton);
begin
  case AButton.BAlign of
    0 : AButton.Align := TAlign.alNone;
    1 : AButton.Align := TAlign.alTop;
    2 : AButton.Align := TAlign.alLeft;
    3 : AButton.Align := TAlign.alRight;
    4 : AButton.Align := TAlign.alBottom;
    else
      AButton.Align := TAlign.alNone;
  end;
end;

procedure TfrmSetup.btnAddClick(Sender: TObject);
var
  LFlowPanel : TPanel;
  LButton : TMyButton;
  LName : String;
  LComboBox : TComboBox;
begin
  LFlowPanel := TPanel(self.FindComponent(StringReplace(TButton(Sender).Name,'buttonAdd','actionBar',[])));
  LName := StringReplace(TButton(Sender).Name,'buttonAdd','actionBarButton_'+IntToStr(LFlowPanel.ControlCount),[]);
  LComboBox := TComboBox(self.FindComponent(StringReplace(TButton(Sender).Name,'buttonAdd','cbActionBarAlign',[])));
  LButton := TMyButton.Create(self);
  LButton.Parent := LFlowPanel;
  LButton.Name := LName;
  LButton.Caption := 'N';
  if LComboBox.ItemIndex = 1 then
    LButton.Left := Round(LFlowPanel.Width / 2) //Scale(60)
  else
    LButton.Top  := Round(LFlowPanel.Height / 2);
  LButton.Height := Scale(30);
  LButton.Width := Scale(30);
  LButton.OnClick := btnPropertyClick;
  LButton.AlignWithMargins := true;
  LButton.OnClick(LButton);
end;

procedure TfrmSetup.btnChangeFontClick(Sender: TObject);
var
  dlgFont : TFontDialog;
  LFont : TFont;
begin
  dlgFont := TFontDialog.Create(Self);
  if Assigned(TMyButton(Sender).TagObject) then
    LFont := TFont(TMyButton(Sender).TagObject)
  else
  begin
    LFont := TFont.Create;
    LFont.Name := Font.Name;
    LFont.Size := Font.Size;
    LFont.Color := Font.Color;
    LFont.Style := Font.Style;
    LFont.Charset := Font.Charset;
  end;
  dlgFont.Font.Name := LFont.Name;
  dlgFont.Font.Size := LFont.Size;
  dlgFont.Font.Color := LFont.Color;
  dlgFont.Font.Style := LFont.Style;
  dlgFont.Font.Charset := LFont.Charset;
  if dlgFont.Execute then
  begin
    LFont.Name     := dlgFont.Font.Name;
    LFont.Size     := dlgFont.Font.Size;
    LFont.Color    := dlgFont.Font.Color;
    LFont.Style    := dlgFont.Font.Style;
    LFont.Charset  := dlgFont.Font.Charset;
    TMyButton(Sender).TagObject := TObject(LFont);
    if ContainsText(TMyButton(Sender).Name, 'btnfont') then
      TLabeledEdit(self.FindComponent(StringReplace(
        TMyButton(Sender).Name,'btnfont','edTitFont',[]))).Text  :=
        LFont.Name+','+LFont.Size.ToString+','+ColorToString(LFont.Color)
    else
      TLabeledEdit(self.FindComponent(StringReplace(
        TMyButton(Sender).Name,'btnUnselectfont','edUnselectFont',[]))).Text  :=
        LFont.Name+','+LFont.Size.ToString+','+ColorToString(LFont.Color);
  end
end;

procedure TfrmSetup.btnPathClick(Sender: TObject);
var
  Name: String;
  edit: TLabeledEdit;
begin
  if OpenDialog.Execute then
  begin
    Name := TButton(Sender).Name;
    Name := ReplaceStr(Name, 'btnPath_', 'edPath_');
    edit := TLabeledEdit(self.FindComponent(Name));
    edit.Text := OpenDialog.FileName;
  end;
end;

procedure TfrmSetup.btnPropertyClick(Sender: TObject);
var
  LButton : TButton;
begin
  LButton := TButton(Sender);
  if not Assigned(FormProperty) then
    FormProperty := TFormProperty.Create(self);
  FormProperty.ActiveControl := Sender;
  TimerShowProperty.Enabled := True;
end;

procedure TfrmSetup.btnSaveClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  LockWindowUpdate(Self.Handle);

  SaveSettings;
  GenerateGrid;
  LoadProgramsInfo;

  LockWindowUpdate(0);
  Screen.Cursor := crDefault;
end;

procedure TfrmSetup.btnReloadClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  LockWindowUpdate(Self.Handle);
  GenerateGrid;
  LoadProgramsInfo;
  LockWindowUpdate(0);
  Screen.Cursor := crDefault;
end;

procedure TfrmSetup.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmSetup.ButtonAddGeneralActionButtonClick(Sender: TObject);
var
  LButton : TMyButton;
  LName : String;
begin
  LName := 'generalActionBarButton_'+IntToStr(FlowPanelGeneral.ControlCount);
  LButton := TMyButton.Create(self);
  LButton.Parent := FlowPanelGeneral;
  LButton.Name := LName;
  LButton.Caption := 'N';
  if ComboBoxGeneralPlacement.ItemIndex = 1 then
    LButton.Left := Round(FlowPanelGeneral.Width / 2) //Scale(60)
  else
    LButton.Top  := Round(FlowPanelGeneral.Height / 2); //Scale(60);
  LButton.Height := Scale(30);
  LButton.Width := Scale(30);
  LButton.OnClick := btnPropertyClick;

end;

procedure TfrmSetup.CheckBoxGeneralShowClick(Sender: TObject);
begin
  FlowPanelGeneral.Visible := CheckBoxGeneralShow.Checked;
end;

function TfrmSetup.checkPass: Boolean;
var
  Pass: String;
begin
  Pass := Trim(FIniFile.ReadString(S_LOGIN, K_PASS, DEF_PASS));
  if Pass = '' then Pass := DEF_PASS;

  if Pass <> DEF_PASS then
     Pass := Encrip_Decrip(Pass);

  Result := (Pass = Trim(edLogin.Text));
end;

procedure TfrmSetup.ComboBoxGeneralPlacementChange(Sender: TObject);
var
  LPrevAlign : TAlign;
begin
  LPrevAlign := FlowPanelGeneral.Align;
  case TComboBox(Sender).ItemIndex of
    0: FlowPanelGeneral.Align := TAlign.alLeft;
    1: FlowPanelGeneral.Align := TAlign.alBottom;
    2: FlowPanelGeneral.Align := TAlign.alRight;
  end;
  if (LPrevAlign <> FlowPanelGeneral.Align) then
  begin
    if LPrevAlign = TAlign.alBottom then
    begin
      RealignPanelButtons(FlowPanelGeneral);
    end
    else if FlowPanelGeneral.Align = TAlign.alBottom then
    begin
      RealignPanelButtons(FlowPanelGeneral);
    end;
  end;
end;

function TfrmSetup.Encrip_Decrip(Pass: String): String;
var
  Index: Integer;
  sPass: String;
begin
  //simple encryptor reversing the bits 1 4 5 6 (10011100 = 57)
  sPass := Pass;
  for Index := 1 to Length(sPass) do
      sPass[Index] := Chr(Ord(sPass[Index]) Xor $57);

  Result := sPass;
end;

procedure TfrmSetup.edLoginKeyPress(Sender: TObject; var Key: Char);
begin
  if ord(Key) = VK_ESCAPE then Close
  else begin
    if ord(Key) = VK_RETURN then
       if CheckPass then
       begin
         Screen.Cursor := crHourGlass;
         LockWindowUpdate(Self.Handle);

         LoadProgramsInfo;
         pnlPass.Visible := False;
         pnlGral.Visible := True;
         gridPanel.Visible := True;

         LockWindowUpdate(0);
         Screen.Cursor := crDefault;
       end
       else begin
         MessageDlg('Password is incorrect', mtError, [mbOK], 0);
         Close;
       end;
  end;
end;

function TfrmSetup.FindAppPanel(Col, Row: Integer): TPanel;
begin
  Result := TPanel(self.FindComponent('appPanel_'+IntToStr(Col)+'_'+IntToStr(Row)));
end;

procedure TfrmSetup.FormCreate(Sender: TObject);
begin
   TimerShowProperty.Enabled := false;
   FIniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
   GenerateGrid;

   pnlGral.Visible := False;
   gridPanel.Visible := False;
   pnlPass.Top  := (self.Height div 2) - pnlPass.Height;
   pnlPass.Left := (self.Width div 2) - (pnlPass.Width div 2);
   pnlPass.Visible := True;
end;

procedure TfrmSetup.FormDestroy(Sender: TObject);
begin
  FIniFile.Free;
end;

procedure TfrmSetup.GenerateGrid;
var
  mainPanel, otherPanel: TPanel;
  MarginL, MarginR: Integer;
  MarginT, MarginB: Integer;
  iH, iW, LCount, LIndex: Integer;
  sIndex : string;
  cItem: TCellItem;
  AutoSize: Boolean;
  edit: TLabeledEdit;
  button: TMyButton;
  check: TCheckBox;
  LLabel : TLabel;
  LSpinEdit : TSpinEdit;
  LColorBox : TColorBox;
  LIdleInsecs : integer;
  LComboBox : TComboBox;
  LRadioGroup : TRadioGroup;
  LFlowPanel : TPanel;
  LBitButton : TButton;
  appPanel : TScrollBox;
  LButton : TMyButton;
begin
  //grid section
  FgWidth  := FIniFile.ReadInteger(S_GRID, K_WIDTH, 2);
  FgHeight := FIniFile.ReadInteger(S_GRID, K_HEIGHT, 2);
  MarginL  := FIniFile.ReadInteger(S_GRID, K_MARGINL, 0);
  MarginR  := FIniFile.ReadInteger(S_GRID, K_MARGINR, 0);
  MarginT  := FIniFile.ReadInteger(S_GRID, K_MARGINT, 0);
  MarginB  := FIniFile.ReadInteger(S_GRID, K_MARGINB, 0);
  LIdleInsecs := FIniFile.ReadInteger(S_GRID, K_IDLETIME , 60);
  AutoSize := FIniFile.ReadString(S_GRID, K_AUTOSIZE, 'Y') = 'Y';
  CheckBoxGeneralShow.Checked := FIniFile.ReadString(S_GRID, K_GENERAL_SHOW, 'N') = 'Y';
  ComboBoxGeneralPlacement.ItemIndex := FIniFile.ReadInteger(S_GRID, K_GENERAL_ALIGN , 1);
  ComboBoxGeneralPlacement.OnChange(ComboBoxGeneralPlacement);
  SpinEditGeneralSize.Value := FIniFile.ReadInteger(S_GRID, K_GENERAL_SIZE , 32);
  CheckBoxGeneralShowClick(CheckBoxGeneralShow);
  EditMQTTServer.Text := FIniFile.ReadString(S_GRID, 'MQTT_Server',  '');
  EditMQTTClientID.Text := FIniFile.ReadString(S_GRID, 'MQTT_ClientID',  '');
  FlowPanelGeneral.Visible := CheckBoxGeneralShow.Checked;
  for LIndex := 0 to FlowPanelGeneral.ControlCount - 1 do
  begin
    LButton := TMyButton(FlowPanelGeneral.Controls[LIndex]);
    LButton.Free;
  end;

  LCount := FIniFile.ReadInteger(S_GRID, 'GeneralActionBarButtons',  0);
  for LIndex := 0 to LCount - 1 do
  begin
    LButton := TMyButton.Create(self);
    sIndex := IntToStr(LIndex);
    LButton.Parent := FlowPanelGeneral;
    LButton.Name := 'generalActionBarButton_'+IntToStr(LIndex);
    LButton.Left := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_Left',  0);
    LButton.Top := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_Top',  0);
    LButton.Width := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_Width',  0);
    LButton.Height := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_Height',  0);
    LButton.Margins.Left := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_LeftMargin', Scale(0));
    LButton.Margins.Top  := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_TopMargin', Scale(0));
    LButton.Margins.Right := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_RightMargin',  Scale(0));
    LButton.Margins.Bottom := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_BottomMargin',  Scale(0));
    LButton.AlignWithMargins := true;
    LButton.Font.Name := FIniFile.ReadString(S_GRID, 'GABButton'+sIndex+'_'+K_FONTNAME, LButton.Font.Name);
    LButton.Font.CharSet := TFontCharSet(FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_'+K_CHARSET, LButton.Font.CharSet));
    LButton.Font.Color := TColor(FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_'+K_FONTCOLOR, LButton.Font.Color));
    LButton.Font.Size := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_'+K_FONTSIZE, LButton.Font.Size);
    LButton.Font.Style := TFontStyles(Byte(FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_'+K_FONTSTYLE, Byte(LButton.Font.Style))));
    LButton.Visible := FIniFile.ReadString(S_GRID, 'GABButton'+sIndex+'_Visible', 'Y') = 'Y';
    LButton.Caption := FIniFile.ReadString(S_GRID, 'GABButton'+sIndex+'_Caption',  '');
    LButton.Gylph := FIniFile.ReadString(S_GRID, 'GABButton'+sIndex+'_Glyph',  '');
    if LButton.Gylph <> '' then
      LButton.OptionsImage.Glyph.LoadFromFile(LButton.Gylph);
    LButton.KeyToSend := FIniFile.ReadString(S_GRID, 'GABButton'+sIndex+'_Key',  '');
    LButton.MQTTdata := FIniFile.ReadString(S_GRID, 'GABButton'+sIndex+'_MQTT',  '');
    LButton.BType := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_BType',  0);
    LButton.BAlign := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_Align',  0);
    AlignButton(LButton);
    LButton.Topic := FIniFile.ReadString(S_GRID, 'GABButton'+sIndex+'_Topic',  '');
    LButton.OnClick := btnPropertyClick;
  end;

  //pass
  edPass.Text := FIniFile.ReadString(S_LOGIN, K_PASS, '');

  //set the edit with the ini info
  edGridWidth.Text    := IntToStr(FgWidth);
  edGridHeight.Text   := IntToStr(FgHeight);
  edMarginTop.Text    := IntToStr(MarginT);
  edMarginBottom.Text := IntToStr(MarginB);
  edMarginLeft.Text   := IntToStr(MarginL);
  edMarginRight.Text  := IntToStr(MarginL);
  SpinEditIdle.Value  := LIdleInsecs;
  ckAutoSize.Checked  := AutoSize;

  gridPanel.RowCollection.BeginUpdate;
  gridPanel.ColumnCollection.BeginUpdate;

  //remove posible controls and clear controls
  for iH := 0 to gridPanel.ControlCount-1 do
    GridPanel.Controls[0].Free;
  GridPanel.RowCollection.Clear;
  GridPanel.ColumnCollection.Clear;

  //create row/col to gridPanel
  for iH := 1 to FgHeight do
  begin
    cItem := gridPanel.RowCollection.Add;
    cItem.SizeStyle := ssPercent;
    cItem.Value := 100/FgHeight;
  end;

  for iW := 1 to FgWidth do
  begin
    cItem := gridPanel.ColumnCollection.Add;
    cItem.SizeStyle := ssPercent;
    cItem.Value := 100/FgWidth;
  end;

  //create box panel
  for iH := 0 to FgHeight-1 do
  begin
    for iW := 0 to FgWidth-1 do
    begin
      //main panel for program
      mainPanel := TPanel.Create(self);
      mainPanel.Parent := gridPanel;
      mainPanel.Name := 'mainPanel_'+IntToStr(iH)+'_'+IntToStr(iW);
      mainPanel.BevelInner := bvRaised;
      mainPanel.BevelOuter := bvLowered;
      mainPanel.Align := alClient;
      mainPanel.Caption := '';

      //set margins
      otherPanel := TPanel.Create(self);
      otherPanel.Parent := mainPanel;
      otherPanel.Name := 'bPanel_'+IntToStr(iH)+'_'+IntToStr(iW);
      otherPanel.Width := 1;
      if MarginB = 0 then
      begin
        otherPanel.Visible := False;
        otherPanel.Height := 1; //0 is not possible
      end
      else otherPanel.Height := Scale(MarginB);
      otherPanel.BevelOuter := bvNone;
      otherPanel.Align := alBottom;
      otherPanel.Caption := '';

      otherPanel := TPanel.Create(self);
      otherPanel.Parent := mainPanel;
      otherPanel.Name := 'lPanel_'+IntToStr(iH)+'_'+IntToStr(iW);
      otherPanel.Height := 1;
      if MarginL = 0 then
      begin
        otherPanel.Visible := False;
        otherPanel.Width := 1; //0 is not possible
      end
      else otherPanel.Width := Scale(MarginL);
      otherPanel.BevelOuter := bvNone;
      otherPanel.Align := alLeft;
      otherPanel.Caption := '';

      otherPanel := TPanel.Create(self);
      otherPanel.Parent := mainPanel;
      otherPanel.Name := 'rPanel_'+IntToStr(iH)+'_'+IntToStr(iW);
      otherPanel.Height := 1;
      if MarginR = 0 then
      begin
        otherPanel.Visible := False;
        otherPanel.Width := 1; //0 is not possible
      end
      else otherPanel.Width := Scale(MarginR);
      otherPanel.BevelOuter := bvNone;
      otherPanel.Align := alRight;
      otherPanel.Caption := '';

      otherPanel := TPanel.Create(self);
      otherPanel.Parent := mainPanel;
      otherPanel.Name := 'tPanel_'+IntToStr(iH)+'_'+IntToStr(iW);
      otherPanel.Width := 1;
      if MarginT = 0 then
      begin
        otherPanel.Visible := False;
        otherPanel.Height := 1; //0 is not possible
      end
      else otherPanel.Height := Scale(MarginT);
      otherPanel.BevelOuter := bvNone;
      otherPanel.Align := alTop;
      otherPanel.Caption := '';

      //left and right space panels
      otherPanel := TPanel.Create(self);
      otherPanel.Parent := mainPanel;
      otherPanel.BevelOuter := bvNone;
      otherPanel.Align := alLeft;
      otherPanel.Caption := '';
      otherPanel.Width := Scale(20);

      otherPanel := TPanel.Create(self);
      otherPanel.Parent := mainPanel;
      otherPanel.BevelOuter := bvNone;
      otherPanel.Align := alRight;
      otherPanel.Caption := '';
      otherPanel.Width := Scale(20);

      //app panel
      appPanel := TScrollBox.Create(Self);//TPanel.Create(self);
      appPanel.Parent := mainPanel;
      appPanel.BevelOuter := bvNone;
      appPanel.Name := 'appPanel_'+IntToStr(iH)+'_'+IntToStr(iW);
      appPanel.Align := alClient;
      appPanel.BorderStyle := bsNone;

      //title panel
      otherPanel := TPanel.Create(self);
      otherPanel.Parent := appPanel;
      otherPanel.Name := 'titPanel_'+IntToStr(iH)+'_'+IntToStr(iW);
      otherPanel.Height := Scale(22);
      otherPanel.BevelOuter := bvNone;
      otherPanel.Align := alTop;
      otherPanel.Caption := '';

      //edits - button - checks
      //App Title
      edit := TLabeledEdit.Create(self);
      edit.Parent := appPanel;
      edit.Name := 'edTit_'+IntToStr(iH)+'_'+IntToStr(iW);
      edit.EditLabel.Caption := 'App Title:';
      edit.LabelPosition := lpLeft;
      edit.Font.Size := 9;
      edit.Height := Scale(19);
      edit.Width := Scale(310);
      edit.Top := Scale(25);
      edit.left := Scale(60);

      //App Name
      edit := TLabeledEdit.Create(self);
      edit.Parent := appPanel;
      edit.Name := 'edName_'+IntToStr(iH)+'_'+IntToStr(iW);
      edit.EditLabel.Caption := 'App Name:';
      edit.LabelPosition := lpLeft;
      edit.Height := Scale(19);
      edit.Width := Scale(310);
      edit.Top := Scale(55);
      edit.left := Scale(60);

      //App Class
      edit := TLabeledEdit.Create(self);
      edit.Parent := appPanel;
      edit.Name := 'edClass_'+IntToStr(iH)+'_'+IntToStr(iW);
      edit.EditLabel.Caption := 'App Class:';
      edit.LabelPosition := lpLeft;
      edit.Height := Scale(19);
      edit.Width := Scale(310);
      edit.Top := Scale(85);
      edit.left := Scale(60);

      //App Path edit+button
      edit := TLabeledEdit.Create(self);
      edit.Parent := appPanel;
      edit.Name := 'edPath_'+IntToStr(iH)+'_'+IntToStr(iW);
      edit.EditLabel.Caption := 'App Path:';
      edit.LabelPosition := lpLeft;
      edit.Height := Scale(19);
      edit.Width := Scale(230);
      edit.Top := Scale(115);
      edit.left := Scale(60);

      LBitButton := TButton.Create(self);
      LBitButton.Parent := appPanel;
      LBitButton.Name := 'btnPath_'+IntToStr(iH)+'_'+IntToStr(iW);
      LBitButton.Caption := 'Search Path';
      LBitButton.Top := Scale(114);
      LBitButton.Height := Scale(22);
      LBitButton.Width := Scale(75);
      LBitButton.Left := Scale(60) + Scale(230) + Scale(5);
      LBitButton.OnClick := btnPathClick;

      //App Params edit
      edit := TLabeledEdit.Create(self);
      edit.Parent := appPanel;
      edit.Name := 'edParams_'+IntToStr(iH)+'_'+IntToStr(iW);
      edit.EditLabel.Caption := 'Params:';
      edit.LabelPosition := lpLeft;
      edit.Height := Scale(19);
      edit.Width := Scale(310);
      edit.Top := Scale(145);
      edit.left := Scale(60);

      //Check ShowCaption
      check := TCheckBox.Create(self);
      check.Parent := appPanel;
      check.Name := 'ckShowCaption_'+IntToStr(iH)+'_'+IntToStr(iW);
      check.Caption := 'Show Caption';
      check.Top := Scale(175);
      check.Left := Scale(10);

      //Check ShowTittle
      check := TCheckBox.Create(self);
      check.Parent := appPanel;
      check.Name := 'ckShowTittle_'+IntToStr(iH)+'_'+IntToStr(iW);
      check.Caption := 'Show Tittle';
      check.Top := Scale(175);
      check.Left := Scale(110);

      //Check ShowIcons
      check := TCheckBox.Create(self);
      check.Parent := appPanel;
      check.Name := 'ckShowIcons_'+IntToStr(iH)+'_'+IntToStr(iW);
      check.Caption := 'Show Icons';
      check.Top := Scale(175);
      check.Left := Scale(200);
      //Title Font
      edit := TLabeledEdit.Create(self);
      edit.Parent := appPanel;
      edit.Name := 'edTitFont_'+IntToStr(iH)+'_'+IntToStr(iW);
      edit.EditLabel.Caption := 'Selected:';
      edit.LabelPosition := lpLeft;
      edit.ReadOnly := True;
      edit.Height := Scale(19);
      edit.Width := Scale(230);
      edit.Top := Scale(205);
      edit.left := Scale(60);

      button := TMyButton.Create(self);
      button.Parent := appPanel;
      button.Name := 'btnfont_'+IntToStr(iH)+'_'+IntToStr(iW);
      button.Caption := 'Change Font';
      button.Top := Scale(204);
      button.Height := Scale(22);
      button.Width := Scale(75);
      button.Left := Scale(60) + Scale(230) + Scale(5);

      button.OnClick := btnChangeFontClick;

      //Unselected Title
      //Title Font
      edit := TLabeledEdit.Create(self);
      edit.Parent := appPanel;
      edit.Name := 'edUnselectFont_'+IntToStr(iH)+'_'+IntToStr(iW);
      edit.EditLabel.Caption := 'Unselected:';
      edit.LabelPosition := lpLeft;
      edit.ReadOnly := True;
      edit.Height := Scale(19);
      edit.Width := Scale(230);
      edit.Top := Scale(235);
      edit.left := Scale(60);

      button := TMyButton.Create(self);
      button.Parent := appPanel;
      button.Name := 'btnUnselectfont_'+IntToStr(iH)+'_'+IntToStr(iW);
      button.Caption := 'Change Font';
      button.Top := Scale(234);
      button.Height := Scale(22);
      button.Width := Scale(75);
      button.Left := Scale(60) + Scale(230) + Scale(5);

      button.OnClick := btnChangeFontClick;

      //Title Size
      LLabel := TLabel.Create(self);
      LLabel.Parent := appPanel;
      LLabel.Name := 'lblTitSize_'+IntToStr(iH)+'_'+IntToStr(iW);
      LLabel.Caption := 'Title Size:';
      LLabel.Height := Scale(19);
      LLabel.Width := Scale(50);
      LLabel.Top := Scale(265);
      LLabel.left := Scale(10);

      LSpinEdit := TSpinEdit.Create(self);
      LSpinEdit.Parent := appPanel;
      LSpinEdit.Name := 'seTitSize_'+IntToStr(iH)+'_'+IntToStr(iW);
      LSpinEdit.Top := Scale(265) - Scale(2);
      LSpinEdit.Height := Scale(22);
      LSpinEdit.Width := Scale(75);
      LSpinEdit.Left := Scale(10) + Scale(50);

      //Title Color
      LLabel := TLabel.Create(self);
      LLabel.Parent := appPanel;
      LLabel.Name := 'lblTitColor_'+IntToStr(iH)+'_'+IntToStr(iW);
      LLabel.Caption := 'Title Color:';
      LLabel.Height := Scale(19);
      LLabel.Width := Scale(50);
      LLabel.Top := Scale(265);
      LLabel.left := Scale(60) + Scale(75) + Scale(15);

      LColorBox := TColorBox.Create(self);
      LColorBox.Parent := appPanel;
      LColorBox.Name := 'cbTitColor_'+IntToStr(iH)+'_'+IntToStr(iW);
      LColorBox.Top := Scale(265) - Scale(2);
      LColorBox.Height := Scale(22);
      LColorBox.Width := Scale(150);
      LColorBox.Left := Scale(150) + Scale(50) + Scale(5);

      //Unselected Title Color
      LLabel := TLabel.Create(self);
      LLabel.Parent := appPanel;
      LLabel.Name := 'lblUnTitColor_'+IntToStr(iH)+'_'+IntToStr(iW);
      LLabel.Caption := 'Unselected Title Color:';
      LLabel.Height := Scale(19);
      LLabel.Width := Scale(125);
      LLabel.Top := Scale(295);
      LLabel.left := Scale(10);

      LColorBox := TColorBox.Create(self);
      LColorBox.Parent := appPanel;
      LColorBox.Name := 'cbUnTitColor_'+IntToStr(iH)+'_'+IntToStr(iW);
      LColorBox.Top := Scale(295) - Scale(2);
      LColorBox.Height := Scale(22);
      LColorBox.Width := Scale(150);
      LColorBox.Left := Scale(125) + Scale(5);

      //Check Show App Action Bar
      check := TCheckBox.Create(self);
      check.Parent := appPanel;
      check.Name := 'ckShowActionBar_'+IntToStr(iH)+'_'+IntToStr(iW);
      check.Caption := 'Show Action Bar';
      check.Top := Scale(330);
      check.Left := Scale(10);
      check.OnClick := ActionBarVisibleChange;

      //Action Bar Size
      LLabel := TLabel.Create(self);
      LLabel.Parent := appPanel;
      LLabel.Name := 'lblActionBarSize_'+IntToStr(iH)+'_'+IntToStr(iW);
      LLabel.Caption := 'Action Bar Size:';
      LLabel.Height := Scale(19);
      LLabel.Width := Scale(80);
      LLabel.Top := Scale(360);
      LLabel.left := Scale(10);

      LSpinEdit := TSpinEdit.Create(self);
      LSpinEdit.Parent := appPanel;
      LSpinEdit.Name := 'seActionBarSize_'+IntToStr(iH)+'_'+IntToStr(iW);
      LSpinEdit.Top := Scale(360) - Scale(2);
      LSpinEdit.Height := Scale(22);
      LSpinEdit.Width := Scale(65);
      LSpinEdit.Left := Scale(10) + Scale(80);
      LSpinEdit.OnChange := ActionBarSizeChange;

      //Placement of Action Bar
      LLabel := TLabel.Create(self);
      LLabel.Parent := appPanel;
      LLabel.Name := 'lblPlacementOfActionBar_'+IntToStr(iH)+'_'+IntToStr(iW);
      LLabel.Caption := 'Placement of Action Bar:';
      LLabel.Height := Scale(19);
      LLabel.Width := Scale(125);
      LLabel.Top := Scale(360);
      LLabel.left := Scale(10) + Scale(80) + Scale(65) + Scale(10);

      LComboBox := TComboBox.Create(self);
      LComboBox.Parent := appPanel;
      LComboBox.Name := 'cbActionBarAlign_'+IntToStr(iH)+'_'+IntToStr(iW);
      LComboBox.Items.Add('Left');
      LComboBox.Items.Add('Bottom');
      LComboBox.Items.Add('Right');
      LComboBox.Top := Scale(360) - Scale(2);
      LComboBox.Height := Scale(22);
      LComboBox.Width := Scale(65);
      LComboBox.Style := csDropDownList;
      LComboBox.Left := Scale(80) + Scale(65) + Scale(10) + Scale(125) + Scale(10);
      LComboBox.OnChange := ActionBarPlacementChange;

      //Radio Group
      LRadioGroup := TRadioGroup.Create(self);
      LRadioGroup.Parent := appPanel;
      LRadioGroup.Name := 'rgRadioGroupStartOption_'+IntToStr(iH)+'_'+IntToStr(iW);
      LRadioGroup.Items.Add('Auto');
      LRadioGroup.Items.Add('Manual');
      LRadioGroup.Items.Add('Last State');
      LRadioGroup.Caption := 'Start Options';
      LRadioGroup.Top := Scale(390);
      LRadioGroup.Height := Scale(50);
      LRadioGroup.Width := Scale(250);
      LRadioGroup.Columns := 3;
      LRadioGroup.Left := Scale(10);

      LBitButton := TButton.Create(self);
      LBitButton.Parent := appPanel;
      LBitButton.Name := 'buttonAdd_'+IntToStr(iH)+'_'+IntToStr(iW);
      //LBitButton.Align := alBottom;
      LBitButton.Caption := 'Add Button in Action Bar';
      LBitButton.Height := Scale(30);
      LBitButton.Width := Scale(250);
      LBitButton.Top := Scale(450);
      LBitButton.OnClick := btnAddClick;

      //Action Bar panel
      LFlowPanel := TPanel.Create(self);
      LFlowPanel.Parent := mainPanel;
      LFlowPanel.Name := 'actionBar_'+IntToStr(iH)+'_'+IntToStr(iW);
      LFlowPanel.Align := alBottom;
      LFlowPanel.Caption := '';
      LFlowPanel.Height := Scale(32);
      LFlowPanel.BevelInner := bvRaised;
      LFlowPanel.BevelOuter := bvLowered;
      LFlowPanel.ParentBackground := false;
      LFlowPanel.Tag := 0;  //here stored the program handle

    end;
  end;

  gridPanel.RowCollection.EndUpdate;
  gridPanel.ColumnCollection.EndUpdate;

  //update minimun Constraints. If I do not restrict, components of each box may be lost.
  self.Constraints.MinHeight := Scale(pnlGral.Height + (FgHeight * (165+22+MarginT+MarginB+20)));
  self.Constraints.MinWidth  := Scale(FgWidth * (310+60+MarginL+MarginR+40));
end;


procedure TfrmSetup.LoadProgramsInfo;
var
  iW, iH, LIndex, LCount, LActCount, LMQTTCount: Integer;
  sSection, sPartName, sIndex: String;
  pName, pClass, pTitle: String;
  pPath, pParams, pTitleFont: String;
  pShowCaption, pShowTitle, pShowIcons: Boolean;
  LFont, LUnselectFont, LPanelFont : TFont;
  panel: TPanel;
  LPanelHeight, LPanelSize : Integer;
  LPanelColor : TColor;
  LComboBox : TComboBox;
  LButton : TMyButton;
  MQTTDetails : TMQTTDetails;
begin
  //read programs info, only those configured will appear
  for iH := 0 to FgHeight-1 do
  begin
    for iW := 0 to FgWidth-1 do
    begin
      sPartName := '_'+IntToStr(iH)+'_'+IntToStr(iW);
      sSection := S_PROGRAM + sPartName;
      LFont := TFont.Create;
      LUnselectFont := TFont.Create;
      panel := TPanel(self.FindComponent('titPanel_'+IntToStr(iH)+'_'+IntToStr(iW)));
      LPanelFont := panel.Font;
      if FIniFile.SectionExists(sSection) then
      begin
        pName        := FIniFile.ReadString(sSection, K_NAME, '');
        pClass       := FIniFile.ReadString(sSection, K_CLASS, '');
        pTitle       := FIniFile.ReadString(sSection, K_TITLE, '');
        pPath        := FIniFile.ReadString(sSection, K_APP, '');
        pParams      := FIniFile.ReadString(sSection, K_PARAMS, '');
        pShowCaption := FIniFile.ReadString(sSection, K_SHOWCAPTION, 'Y') = 'Y';
        pShowTitle   := FIniFile.ReadString(sSection, K_SHOWTITLE, 'Y') = 'Y';
        pShowIcons   := FIniFile.ReadString(sSection, K_SHOWICONS, 'Y') = 'Y';
        LFont.Name    := FIniFile.ReadString(sSection, K_FONTNAME, LPanelFont.Name);
        LFont.CharSet := TFontCharSet(FIniFile.ReadInteger(sSection, K_CHARSET, LPanelFont.CharSet));
        LFont.Color   := TColor(FIniFile.ReadInteger(sSection, K_FONTCOLOR, LPanelFont.Color));
        LFont.Size    := FIniFile.ReadInteger(sSection, K_FONTSIZE, LPanelFont.Size);
        LFont.Style   := TFontStyles(Byte(FIniFile.ReadInteger(sSection, K_FONTSTYLE, Byte(LPanelFont.Style))));

        TLabeledEdit(self.FindComponent('edTit'+sPartName)).Text   := pTitle;
        TLabeledEdit(self.FindComponent('edName'+sPartName)).Text  := pName;
        TLabeledEdit(self.FindComponent('edClass'+sPartName)).Text := pClass;
        TLabeledEdit(self.FindComponent('edPath'+sPartName)).Text  := pPath;
        TLabeledEdit(self.FindComponent('edParams'+sPartName)).Text:= pParams;
        TCheckBox(self.FindComponent('ckShowCaption'+sPartName)).Checked := pShowCaption;
        TCheckBox(self.FindComponent('ckShowTittle'+sPartName)).Checked  := pShowTitle;
        TCheckBox(self.FindComponent('ckShowIcons'+sPartName)).Checked   := pShowIcons;
        TMyButton(self.FindComponent('btnfont'+sPartName)).TagObject  := TObject(LFont);
        TLabeledEdit(self.FindComponent('edTitFont'+sPartName)).Text  := LFont.Name+','+LFont.Size.ToString+','+ColorToString(LFont.Color);
        LUnselectFont.Name    := FIniFile.ReadString(sSection, K_FONTNAME_UN, LPanelFont.Name);
        LUnselectFont.CharSet := TFontCharSet(FIniFile.ReadInteger(sSection, K_CHARSET_UN, LPanelFont.CharSet));
        LUnselectFont.Color   := TColor(FIniFile.ReadInteger(sSection, K_FONTCOLOR_UN, LPanelFont.Color));
        LUnselectFont.Size    := FIniFile.ReadInteger(sSection, K_FONTSIZE_UN, LPanelFont.Size);
        LUnselectFont.Style   := TFontStyles(Byte(FIniFile.ReadInteger(sSection, K_FONTSTYLE_UN, Byte(LPanelFont.Style))));
        TMyButton(self.FindComponent('btnUnselectfont'+sPartName)).TagObject  := TObject(LUnselectFont);
        TLabeledEdit(self.FindComponent('edUnselectFont'+sPartName)).Text  :=
          LUnselectFont.Name+','+LUnselectFont.Size.ToString+','
          +ColorToString(LUnselectFont.Color);

        LPanelHeight := FIniFile.ReadInteger(sSection, 'TitleSize', panel.Height);
        LPanelColor  := TColor(FIniFile.ReadInteger(sSection, 'BackgroundColor', panel.Color));
        TSpinEdit(self.FindComponent('seTitSize'+sPartName)).Value := LPanelHeight;
        TColorBox(self.FindComponent('cbTitColor'+sPartName)).Selected := LPanelColor;
        LPanelColor  := TColor(FIniFile.ReadInteger(sSection, 'UnSelectedColor', panel.Color));
        TColorBox(self.FindComponent('cbUnTitColor'+sPartName)).Selected := LPanelColor;

        TCheckBox(self.FindComponent('ckShowActionBar'+sPartName)).Checked  := FIniFile.ReadString(sSection, 'ActionBar', 'N') = 'Y';
        LPanelSize := FIniFile.ReadInteger(sSection, 'ActionBarSize', 25);
        TSpinEdit(self.FindComponent('seActionBarSize'+sPartName)).Value := LPanelSize;
        LComboBox := TComboBox(self.FindComponent('cbActionBarAlign'+sPartName));
        LComboBox.ItemIndex := FIniFile.ReadInteger(sSection, 'ActionBarAlign', 1);
        LComboBox.OnChange(LComboBox);
        TRadioGroup(self.FindComponent('rgRadioGroupStartOption'+sPartName)).ItemIndex := FIniFile.ReadInteger(sSection, 'StartOption', 0);

        panel.Height := 20;
        panel.Color  := LPanelColor;
        SetTitlePanel(iH, iW, pTitle, True, LFont);

        //For Buttons in Action Bar
        panel := TPanel(self.FindComponent('actionBar'+sPartName));
        panel.Height := Scale(LPanelSize);
        panel.Width := Scale(LPanelSize);
        LCount := 2;
        for LIndex := LCount to panel.ControlCount - 1 do
        begin
          LButton := TMyButton(panel.Controls[LIndex]);
          LButton.Free;
        end;
        LCount := FIniFile.ReadInteger(sSection, 'ActionBarButtons',  0);
        for LIndex := 0 to LCount - 1 do
        begin
          sIndex := IntToStr(LIndex);
          LButton := TMyButton.Create(self);
          LButton.Parent := panel;
          LButton.Name := 'actionBarButton_'+sPartName+IntToStr(LIndex);
          LButton.Caption := FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_Caption',  'N');
          LButton.Left := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_Left', Scale(60));
          LButton.Top  := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_Top', Scale(60));
          LButton.Width := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_Width',  Scale(30));
          LButton.Height := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_Height',  Scale(30));

          LButton.Margins.Left := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_LeftMargin', Scale(0));
          LButton.Margins.Top  := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_TopMargin', Scale(0));
          LButton.Margins.Right := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_RightMargin',  Scale(0));
          LButton.Margins.Bottom := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_BottomMargin',  Scale(0));
          LButton.AlignWithMargins := true;
          LButton.Font.Name := FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_'+K_FONTNAME, LPanelFont.Name);
          LButton.Font.CharSet := TFontCharSet(FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_'+K_CHARSET, LPanelFont.CharSet));
          LButton.Font.Color   := TColor(FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_'+K_FONTCOLOR, LPanelFont.Color));
          LButton.Font.Size    := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_'+K_FONTSIZE, LPanelFont.Size);
          LButton.Font.Style   := TFontStyles(Byte(FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_'+K_FONTSTYLE, Byte(LPanelFont.Style))));
          LButton.Visible := FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_Visible', 'Y') = 'Y';
          LButton.Gylph :=  FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_Glyph',  '');
          if LButton.Gylph <> '' then
            LButton.OptionsImage.Glyph.LoadFromFile(LButton.Gylph);
          LButton.KeyToSend := FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_Key',  '');
          LButton.MQTTData := FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_MQTT',  '');
          LButton.BType := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_BType',  0);
          LButton.OnClick := btnPropertyClick;
          LButton.BAlign := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_Align',  0);
          if (LButton.BAlign = 1) and (LButton.Top > 10) then
            LButton.Top := LButton.Top + 1;
          if (LButton.BAlign = 2) and (LButton.Left > 10) then
            LButton.Left := LButton.Left + 1;
          AlignButton(LButton);
          LButton.Topic := FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_Topic',  '');
          LButton.BMsg := FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_Message',  '');
          LButton.BPwd := FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_Pwd',  '');
          LActCount := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_MQTTCount',  0);

          for LMQTTCount := 0 to LActCount-1 do
          begin
            MQTTDetails := TMQTTDetails.Create('','',0);
            LButton.MQTTInfo.AddObject(IntToStr(LButton.MQTTInfo.Count + 1),TObject(MQTTDetails));
            TMQTTDetails(LButton.MQTTInfo.Objects[LMQTTCount]).MQTTData :=
              FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_MQTTData'+IntToStr(LMQTTCount),  '');
            TMQTTDetails(LButton.MQTTInfo.Objects[LMQTTCount]).Topic :=
              FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_Topic'+IntToStr(LMQTTCount),  '');
            TMQTTDetails(LButton.MQTTInfo.Objects[LMQTTCount]).Delay :=
              FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_Delay'+IntToStr(LMQTTCount),  0);
          end;

        end;

      end
      else begin
        //box without program settings
        TLabeledEdit(self.FindComponent('edTit'+sPartName)).Text   := '';
        TLabeledEdit(self.FindComponent('edName'+sPartName)).Text  := '';
        TLabeledEdit(self.FindComponent('edClass'+sPartName)).Text := '';
        TLabeledEdit(self.FindComponent('edPath'+sPartName)).Text  := '';
        TLabeledEdit(self.FindComponent('edParams'+sPartName)).Text:= '';
        TCheckBox(self.FindComponent('ckShowCaption'+sPartName)).Checked := False;
        TCheckBox(self.FindComponent('ckShowTittle'+sPartName)).Checked  := False;
        TCheckBox(self.FindComponent('ckShowIcons'+sPartName)).Checked   := False;
        TSpinEdit(self.FindComponent('seTitSize'+sPartName)).Value := 20;
        TColorBox(self.FindComponent('cbTitColor'+sPartName)).Selected := clBtnFace;
        TColorBox(self.FindComponent('cbUnTitColor'+sPartName)).Selected := clBtnFace;
        TLabeledEdit(self.FindComponent('edTitFont'+sPartName)).Text  :=
          LPanelFont.Name+','+LPanelFont.Size.ToString+','
          +ColorToString(LPanelFont.Color);
        TLabeledEdit(self.FindComponent('edUnselectFont'+sPartName)).Text  :=
          LPanelFont.Name+','+LPanelFont.Size.ToString+','
          +ColorToString(LPanelFont.Color);

        TCheckBox(self.FindComponent('ckShowActionBar'+sPartName)).Checked  := False;
        TSpinEdit(self.FindComponent('seActionBarSize'+sPartName)).Value := 25;
        TComboBox(self.FindComponent('cbActionBarAlign'+sPartName)).ItemIndex := 1;
        TRadioGroup(self.FindComponent('rgRadioGroupStartOption'+sPartName)).ItemIndex := FIniFile.ReadInteger(sSection, 'StartOption', 0);
        SetTitlePanel(iH, iW, '** no program settigns **', True,LPanelFont);
      end;

    end;  //for W
  end; //for H
end;


procedure TfrmSetup.RealignPanelButtons(APanel: TPanel);
var
  LIndex : integer;
  LButton : TMyButton;
  LHeight, LWidth, LTop, LLeft, LPLeft, LPTop : Integer;
  LBottomM, LRightM, LTopM, LLeftM : Integer;
begin
  LPLeft := 0;
  LPTop := 0;
  for LIndex := 0 to APanel.ControlCount - 1 do
  begin
    LButton := TMyButton(APanel.Controls[LIndex]);
    LLeft := LButton.Left;
    LTop := LButton.Top;
    LWidth := LButton.Width;
    LHeight := LButton.Height;
    LLeftM := LButton.Margins.Left;
    LTopM := LButton.Margins.Top;
    LRightM := LButton.Margins.Right;
    LBottomM := LButton.Margins.Bottom;

    if LButton.BAlign = 1 then
    begin
      LButton.BAlign := 2;
      if LTop > 10 then
        LTop := LTop + 5;
    end
    else if LButton.BAlign = 2 then
    begin
      LButton.BAlign := 1;
      if LLeft > 10 then
        LLeft := LLeft + 5;
    end
    else if LButton.BAlign = 3 then
    begin
      LButton.BAlign := 4;
      if (LLeft >= LPTop) then
        LLeft := LPTop - 5;
    end
    else if LButton.BAlign = 4 then
    begin
      LButton.BAlign := 3;
      if (LTop >= LPLeft) then
        LTop := LPLeft - 5;
      LPTop := LTop;
    end;
//    AlignButton(LButton);
    LButton.Align := TAlign.alNone;
    LButton.Margins.Left := LTopM;
    LButton.Margins.Top := LLeftM;
    LButton.Margins.Right := LBottomM;
    LButton.Margins.Bottom := LRightM;

    LButton.Left := LTop;
    LButton.Top := LLeft;
    LButton.Width := LHeight;
    LButton.Height := LWidth;
    AlignButton(LButton);
    LPTop := LButton.Top;
    LPLeft := LButton.Left;
  end;
end;

procedure TfrmSetup.SaveSettings;
var
  iW, iH, LIndex, LCount, LMQTTCount: Integer;
  sSection, sPartName, sIndex: String;
  pName, pClass, pTitle, pPath: String;
  pShowCaption, pShowTitle, pShowIcons: Boolean;
  LFont : TFont;
  panel: TPanel;
  LButton : TMyButton;
begin
  //delete the sections
  FIniFile.EraseSection(S_LOGIN);
  FIniFile.EraseSection(S_GRID);
  for iH := 0 to FgHeight-1 do
  begin
    for iW := 0 to FgWidth-1 do
      FIniFile.EraseSection(S_PROGRAM + '_'+IntToStr(iH)+'_'+IntToStr(iW));
  end;

  //save pass
  FIniFile.WriteString(S_LOGIN, K_PASS, Encrip_Decrip(Trim(edPass.Text)));

  //Save general settings
  FIniFile.WriteInteger(S_GRID, K_WIDTH, StrToIntDef(edGridWidth.Text, 0));
  FIniFile.WriteInteger(S_GRID, K_HEIGHT, StrToIntDef(edGridHeight.Text, 0));
  FIniFile.WriteInteger(S_GRID, K_MARGINT, StrToIntDef(edMarginTop.Text, 0));
  FIniFile.WriteInteger(S_GRID, K_MARGINB, StrToIntDef(edMarginBottom.Text, 0));
  FIniFile.WriteInteger(S_GRID, K_MARGINL, StrToIntDef(edMarginLeft.Text, 0));
  FIniFile.WriteInteger(S_GRID, K_MARGINR, StrToIntDef(edMarginRight.Text, 0));
  FIniFile.WriteInteger(S_GRID, K_IDLETIME, SpinEditIdle.Value);
  if ckAutoSize.Checked then
     FIniFile.WriteString(S_GRID, K_AUTOSIZE, 'Y')
  else
     FIniFile.WriteString(S_GRID, K_AUTOSIZE, 'N');
  if CheckBoxGeneralShow.Checked then
     FIniFile.WriteString(S_GRID, K_GENERAL_SHOW, 'Y')
  else
     FIniFile.WriteString(S_GRID, K_GENERAL_SHOW, 'N');
  FIniFile.WriteInteger(S_GRID, K_GENERAL_ALIGN, ComboBoxGeneralPlacement.ItemIndex);
  FIniFile.WriteInteger(S_GRID, K_GENERAL_SIZE, SpinEditGeneralSize.Value);
  FIniFile.WriteString(S_GRID, 'MQTT_Server',  EditMQTTServer.Text);
  FIniFile.WriteString(S_GRID, 'MQTT_ClientID',  EditMQTTClientID.Text);

  //For Buttons in General Action Bar

  LCount := 0;
  FIniFile.WriteInteger(S_GRID, 'GeneralActionBarButtons',  FlowPanelGeneral.ControlCount);
  for LIndex := LCount to FlowPanelGeneral.ControlCount - 1 do
  begin
    LButton := TMyButton(FlowPanelGeneral.Controls[LIndex]);
    sIndex := IntToStr(LIndex);
    FIniFile.WriteInteger(S_GRID, 'GABButton'+sIndex+'_Left',  LButton.Left);
    FIniFile.WriteInteger(S_GRID, 'GABButton'+sIndex+'_Top',  LButton.Top);
    FIniFile.WriteInteger(S_GRID, 'GABButton'+sIndex+'_Width',  LButton.Width);
    FIniFile.WriteInteger(S_GRID, 'GABButton'+sIndex+'_Height',  LButton.Height);
    FIniFile.WriteInteger(S_GRID, 'GABButton'+sIndex+'_LeftMargin', LButton.Margins.Left);
    FIniFile.WriteInteger(S_GRID, 'GABButton'+sIndex+'_TopMargin', LButton.Margins.Top);
    FIniFile.WriteInteger(S_GRID, 'GABButton'+sIndex+'_RightMargin', LButton.Margins.Right);
    FIniFile.WriteInteger(S_GRID, 'GABButton'+sIndex+'_BottomMargin', LButton.Margins.Bottom);

    LFont := LButton.Font;
    FIniFile.WriteString(S_GRID, 'GABButton'+sIndex+'_'+K_FONTNAME, LFont.Name);
    FIniFile.WriteInteger(S_GRID, 'GABButton'+sIndex+'_'+K_CHARSET, LFont.CharSet);
    FIniFile.WriteInteger(S_GRID, 'GABButton'+sIndex+'_'+K_FONTCOLOR, LFont.Color);
    FIniFile.WriteInteger(S_GRID, 'GABButton'+sIndex+'_'+K_FONTSIZE, LFont.Size);
    FIniFile.WriteInteger(S_GRID, 'GABButton'+sIndex+'_'+K_FONTSTYLE, Byte(LFont.Style));
    if LButton.Visible then
       FIniFile.WriteString(S_GRID, 'GABButton'+sIndex+'_Visible', 'Y')
    else
       FIniFile.WriteString(S_GRID, 'GABButton'+sIndex+'_Visible', 'N');
    FIniFile.WriteString(S_GRID, 'GABButton'+sIndex+'_Caption',  LButton.Caption);
    FIniFile.WriteString(S_GRID, 'GABButton'+sIndex+'_Glyph',  LButton.Gylph);
    FIniFile.WriteString(S_GRID, 'GABButton'+sIndex+'_Key',  LButton.KeyToSend);
    FIniFile.WriteString(S_GRID, 'GABButton'+sIndex+'_MQTT',  LButton.MQTTData);
    FIniFile.WriteString(S_GRID, 'GABButton'+sIndex+'_Topic',  LButton.Topic);
    FIniFile.WriteInteger(S_GRID, 'GABButton'+sIndex+'_BType',  LButton.BType);
    FIniFile.WriteInteger(S_GRID, 'GABButton'+sIndex+'_Align',  LButton.BAlign);
    FIniFile.WriteString(S_GRID, 'GABButton'+sIndex+'_Topic',  LButton.Topic);
  end;


  //save programs info
  for iH := 0 to FgHeight-1 do
  begin
    for iW := 0 to FgWidth-1 do
    begin
      sPartName := '_'+IntToStr(iH)+'_'+IntToStr(iW);
      sSection := S_PROGRAM + sPartName;

      FIniFile.WriteString(sSection, K_NAME, TLabeledEdit(self.FindComponent('edName'+sPartName)).Text);
      FIniFile.WriteString(sSection, K_CLASS, TLabeledEdit(self.FindComponent('edClass'+sPartName)).Text);
      FIniFile.WriteString(sSection, K_TITLE, TLabeledEdit(self.FindComponent('edTit'+sPartName)).Text);
      FIniFile.WriteString(sSection, K_APP, TLabeledEdit(self.FindComponent('edPath'+sPartName)).Text);
      FIniFile.WriteString(sSection, K_PARAMS, TLabeledEdit(self.FindComponent('edParams'+sPartName)).Text);
      if TCheckBox(self.FindComponent('ckShowCaption'+sPartName)).Checked then
         FIniFile.WriteString(sSection, K_SHOWCAPTION, 'Y')
      else
         FIniFile.WriteString(sSection, K_SHOWCAPTION, 'N');

      if TCheckBox(self.FindComponent('ckShowTittle'+sPartName)).Checked then
         FIniFile.WriteString(sSection, K_SHOWTITLE, 'Y')
      else
         FIniFile.WriteString(sSection, K_SHOWTITLE, 'N');

      if TCheckBox(self.FindComponent('ckShowIcons'+sPartName)).Checked then
         FIniFile.WriteString(sSection, K_SHOWICONS, 'Y')
      else
         FIniFile.WriteString(sSection, K_SHOWICONS, 'N');
      if Assigned(TMyButton(self.FindComponent('btnfont'+sPartName)).TagObject) then
        LFont := TFont(TMyButton(self.FindComponent('btnfont'+sPartName)).TagObject)
      else
      begin
        panel := TPanel(self.FindComponent('titPanel_'+IntToStr(iH)+'_'+IntToStr(iW)));
        LFont := panel.Font;
      end;
      FIniFile.WriteString(sSection, K_FONTNAME, LFont.Name);
      FIniFile.WriteInteger(sSection, K_CHARSET, LFont.CharSet);
      FIniFile.WriteInteger(sSection, K_FONTCOLOR, LFont.Color);
      FIniFile.WriteInteger(sSection, K_FONTSIZE, LFont.Size);
      FIniFile.WriteInteger(sSection, K_FONTSTYLE, Byte(LFont.Style));
      if Assigned(TMyButton(self.FindComponent('btnUnselectfont'+sPartName)).TagObject) then
        LFont := TFont(TMyButton(self.FindComponent('btnUnselectfont'+sPartName)).TagObject)
      else
      begin
        panel := TPanel(self.FindComponent('titPanel_'+IntToStr(iH)+'_'+IntToStr(iW)));
        LFont := panel.Font;
      end;
      FIniFile.WriteString(sSection, K_FONTNAME_UN, LFont.Name);
      FIniFile.WriteInteger(sSection, K_CHARSET_UN, LFont.CharSet);
      FIniFile.WriteInteger(sSection, K_FONTCOLOR_UN, LFont.Color);
      FIniFile.WriteInteger(sSection, K_FONTSIZE_UN, LFont.Size);
      FIniFile.WriteInteger(sSection, K_FONTSTYLE_UN, Byte(LFont.Style));
      FIniFile.WriteInteger(sSection, 'TitleSize', TSpinEdit(self.FindComponent('seTitSize'+sPartName)).Value);
      FIniFile.WriteInteger(sSection, 'BackgroundColor', TColorBox(self.FindComponent('cbTitColor'+sPartName)).Selected);
      FIniFile.WriteInteger(sSection, 'UnSelectedColor', TColorBox(self.FindComponent('cbUnTitColor'+sPartName)).Selected);

      if TCheckBox(self.FindComponent('ckShowActionBar'+sPartName)).Checked then
         FIniFile.WriteString(sSection, 'ActionBar', 'Y')
      else
         FIniFile.WriteString(sSection, 'ActionBar', 'N');
      FIniFile.WriteInteger(sSection, 'ActionBarSize', TSpinEdit(self.FindComponent('seActionBarSize'+sPartName)).Value);
      FIniFile.WriteInteger(sSection, 'ActionBarAlign',  TComboBox(self.FindComponent('cbActionBarAlign'+sPartName)).ItemIndex);
      FIniFile.WriteInteger(sSection, 'StartOption',  TRadioGroup(self.FindComponent('rgRadioGroupStartOption'+sPartName)).ItemIndex);
      SetTitlePanel(iH, iW, TLabeledEdit(self.FindComponent('edTit'+sPartName)).Text, True,LFont);

      //For Buttons in Action Bar
      panel := TPanel(self.FindComponent('actionBar'+sPartName));
      FIniFile.WriteInteger(sSection, 'ActionBarButtons',  panel.ControlCount-LCount);
      for LIndex := 0 to panel.ControlCount - 1 do
      begin
        LButton := TMyButton(panel.Controls[LIndex]);
        sIndex := IntToStr(LIndex);
        FIniFile.WriteInteger(sSection, 'ABButton'+sIndex+'_Left',  LButton.Left);
        FIniFile.WriteInteger(sSection, 'ABButton'+sIndex+'_Top',  LButton.Top);
        FIniFile.WriteInteger(sSection, 'ABButton'+sIndex+'_Width',  LButton.Width);
        FIniFile.WriteInteger(sSection, 'ABButton'+sIndex+'_Height',  LButton.Height);
        FIniFile.WriteInteger(sSection, 'ABButton'+sIndex+'_LeftMargin', LButton.Margins.Left);
        FIniFile.WriteInteger(sSection, 'ABButton'+sIndex+'_TopMargin', LButton.Margins.Top);
        FIniFile.WriteInteger(sSection, 'ABButton'+sIndex+'_RightMargin', LButton.Margins.Right);
        FIniFile.WriteInteger(sSection, 'ABButton'+sIndex+'_BottomMargin', LButton.Margins.Bottom);

        LFont := LButton.Font;
        FIniFile.WriteString(sSection, 'ABButton'+sIndex+'_'+K_FONTNAME, LFont.Name);
        FIniFile.WriteInteger(sSection, 'ABButton'+sIndex+'_'+K_CHARSET, LFont.CharSet);
        FIniFile.WriteInteger(sSection, 'ABButton'+sIndex+'_'+K_FONTCOLOR, LFont.Color);
        FIniFile.WriteInteger(sSection, 'ABButton'+sIndex+'_'+K_FONTSIZE, LFont.Size);
        FIniFile.WriteInteger(sSection, 'ABButton'+sIndex+'_'+K_FONTSTYLE, Byte(LFont.Style));
        if LButton.Visible then
           FIniFile.WriteString(sSection, 'ABButton'+sIndex+'_Visible', 'Y')
        else
           FIniFile.WriteString(sSection, 'ABButton'+sIndex+'_Visible', 'N');
        FIniFile.WriteString(sSection, 'ABButton'+sIndex+'_Caption',  LButton.Caption);
        FIniFile.WriteString(sSection, 'ABButton'+sIndex+'_Glyph',  LButton.Gylph);
        FIniFile.WriteString(sSection, 'ABButton'+sIndex+'_Key',  LButton.KeyToSend);
        FIniFile.WriteString(sSection, 'ABButton'+sIndex+'_MQTT',  LButton.MQTTData);
        FIniFile.WriteInteger(sSection, 'ABButton'+sIndex+'_BType',  LButton.BType);
        FIniFile.WriteInteger(sSection, 'ABButton'+sIndex+'_Align',  LButton.BAlign);
        FIniFile.WriteString(sSection, 'ABButton'+sIndex+'_Topic',  LButton.Topic);
        FIniFile.WriteString(sSection, 'ABButton'+sIndex+'_Message',  LButton.BMsg);
        FIniFile.WriteString(sSection, 'ABButton'+sIndex+'_Pwd',  LButton.BPwd);
        FIniFile.WriteInteger(sSection, 'ABButton'+sIndex+'_MQTTCount',  LButton.MQTTInfo.Count);
        for LMQTTCount := 0 to LButton.MQTTInfo.Count-1 do
        begin
          FIniFile.WriteString(sSection, 'ABButton'+sIndex+'_MQTTData'+IntToStr(LMQTTCount),  TMQTTDetails(LButton.MQTTInfo.Objects[LMQTTCount]).MQTTData);
          FIniFile.WriteString(sSection, 'ABButton'+sIndex+'_Topic'+IntToStr(LMQTTCount),  TMQTTDetails(LButton.MQTTInfo.Objects[LMQTTCount]).Topic);
          FIniFile.WriteInteger(sSection, 'ABButton'+sIndex+'_Delay'+IntToStr(LMQTTCount),  TMQTTDetails(LButton.MQTTInfo.Objects[LMQTTCount]).Delay);
        end;
      end;
    end;  //for W
  end; //for H

  FIniFile.UpdateFile;

  FgHeight := StrToIntDef(edGridHeight.Text, 0);
  FgWidth  := StrToIntDef(edGridWidth.Text, 0);
end;

procedure TfrmSetup.SetTitlePanel(Col, Row: Integer; Title: String; Visible: Boolean; AFont: TFont);
var
  panel: TPanel;
begin
  panel := TPanel(self.FindComponent('titPanel_'+IntToStr(Col)+'_'+IntToStr(Row)));
  if panel <> nil then
  begin
    panel.Visible := Visible;
    panel.Caption := Title;
    panel.Font := AFont;
  end;
end;

procedure TfrmSetup.SpinEditGeneralSizeChange(Sender: TObject);
var
  LFlowPanel : TPanel;
  LComboBox : TComboBox;
begin
  if TSpinEdit(Sender).Text <> '' then
  begin
    if ComboBoxGeneralPlacement.ItemIndex = 1 then
      FlowPanelGeneral.Height := Scale(TSpinEdit(Sender).Value)
    else
      FlowPanelGeneral.Width := Scale(TSpinEdit(Sender).Value)
  end;
end;

procedure TfrmSetup.TimerShowPropertyTimer(Sender: TObject);
begin
  TimerShowProperty.Enabled := false;
  FormProperty.ShowModal;
  if FormProperty.FlagDelete then
  begin
    TMyButton(FormProperty.ActiveControl).Free;
    FormProperty.ActiveControl := nil;
  end;
end;

end.
