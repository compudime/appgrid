object FormProperty: TFormProperty
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Properties'
  ClientHeight = 496
  ClientWidth = 368
  Color = clInactiveCaption
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 368
    Height = 29
    Align = alTop
    Color = clBlue
    ParentBackground = False
    TabOrder = 0
    ExplicitWidth = 356
    object ButtonDelete: TButton
      Left = 275
      Top = 2
      Width = 65
      Height = 25
      Caption = 'Delete'
      TabOrder = 0
      OnClick = ButtonDeleteClick
    end
    object ButtonPrev: TButton
      Left = 16
      Top = 3
      Width = 65
      Height = 25
      Caption = 'Prev'
      TabOrder = 1
      OnClick = ButtonPrevClick
    end
    object ButtonNext: TButton
      Left = 87
      Top = 3
      Width = 65
      Height = 25
      Caption = 'Next'
      TabOrder = 2
      OnClick = ButtonNextClick
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 471
    Width = 368
    Height = 25
    Align = alBottom
    BevelOuter = bvNone
    Color = clInactiveCaption
    ParentBackground = False
    TabOrder = 1
    ExplicitWidth = 356
    object ButtonAddMQTT: TButton
      Left = 226
      Top = 1
      Width = 116
      Height = 25
      Caption = 'Add MQTT'
      TabOrder = 0
      OnClick = ButtonAddMQTTClick
    end
  end
  object ScrollBox: TScrollBox
    Left = 0
    Top = 29
    Width = 368
    Height = 442
    Align = alClient
    BevelEdges = []
    BevelInner = bvNone
    BevelOuter = bvNone
    BorderStyle = bsNone
    TabOrder = 2
    ExplicitWidth = 356
    object Label1: TLabel
      Left = 10
      Top = 168
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Height'
    end
    object Label10: TLabel
      Left = 10
      Top = 190
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Top'
    end
    object Label11: TLabel
      Left = 10
      Top = 212
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Left'
    end
    object Label12: TLabel
      Left = 10
      Top = 122
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Align'
    end
    object Label14: TLabel
      Left = 10
      Top = 232
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Left Margin'
    end
    object Label15: TLabel
      Left = 10
      Top = 256
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Right Margin'
    end
    object Label16: TLabel
      Left = 10
      Top = 278
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Top Margin'
    end
    object Label17: TLabel
      Left = 10
      Top = 300
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Bottom Margin'
    end
    object Label18: TLabel
      Left = 10
      Top = 321
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Message'
    end
    object Label19: TLabel
      Left = 10
      Top = 344
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Password'
    end
    object Label2: TLabel
      Left = 10
      Top = 146
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Width'
    end
    object Label3: TLabel
      Left = 15
      Top = 388
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'MQTT'
    end
    object Label4: TLabel
      Left = 10
      Top = 10
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Type'
    end
    object Label5: TLabel
      Left = 10
      Top = 32
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Caption'
    end
    object Label6: TLabel
      Left = 10
      Top = 55
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Visible'
    end
    object Label7: TLabel
      Left = 10
      Top = 77
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Font'
    end
    object Label8: TLabel
      Left = 10
      Top = 100
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Glyph'
    end
    object Label9: TLabel
      Left = 10
      Top = 366
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Key'
    end
    object LabelTopic: TLabel
      Left = 15
      Top = 410
      Width = 100
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Topic'
    end
    object ComboBoxAlign: TComboBox
      AlignWithMargins = True
      Left = 121
      Top = 118
      Width = 220
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 0
      Text = 'None'
      Items.Strings = (
        'None'
        'Top'
        'Left'
        'Right'
        'Bottom')
    end
    object ComboBoxType: TComboBox
      Left = 120
      Top = 5
      Width = 220
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 1
      Text = 'Custom'
      Items.Strings = (
        'Custom'
        'Play'
        'Stop')
    end
    object ComboBoxVisible: TComboBox
      AlignWithMargins = True
      Left = 121
      Top = 51
      Width = 220
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 2
      Text = 'True'
      Items.Strings = (
        'True'
        'False')
    end
    object EditCaption: TEdit
      AlignWithMargins = True
      Left = 120
      Top = 28
      Width = 220
      Height = 21
      TabOrder = 3
    end
    object EditKeyToSend: TEdit
      AlignWithMargins = True
      Left = 121
      Top = 362
      Width = 220
      Height = 21
      TabOrder = 4
      OnKeyDown = EditKeyToSendKeyDown
      OnKeyPress = EditKeyToSendKeyPress
    end
    object EditMessage: TEdit
      AlignWithMargins = True
      Left = 121
      Top = 318
      Width = 220
      Height = 21
      TabOrder = 5
    end
    object EditMQTT: TEdit
      Left = 121
      Top = 384
      Width = 221
      Height = 21
      TabOrder = 6
    end
    object EditPassword: TEdit
      AlignWithMargins = True
      Left = 121
      Top = 340
      Width = 220
      Height = 21
      TabOrder = 7
    end
    object EditTopic: TEdit
      Left = 121
      Top = 406
      Width = 221
      Height = 21
      TabOrder = 8
    end
    object Panel2: TPanel
      AlignWithMargins = True
      Left = 121
      Top = 96
      Width = 220
      Height = 21
      BevelOuter = bvNone
      TabOrder = 9
      object EditGlyph: TEdit
        Left = 0
        Top = 0
        Width = 171
        Height = 21
        Align = alClient
        ReadOnly = True
        TabOrder = 0
      end
      object ButtonGlyph: TButton
        Left = 171
        Top = 0
        Width = 19
        Height = 21
        Align = alRight
        Caption = '...'
        TabOrder = 1
        OnClick = ButtonGlyphClick
      end
      object Button1: TButton
        Left = 190
        Top = 0
        Width = 30
        Height = 21
        Align = alRight
        Caption = 'Clear'
        TabOrder = 2
        OnClick = Button1Click
      end
    end
    object PanelFont: TPanel
      AlignWithMargins = True
      Left = 121
      Top = 74
      Width = 220
      Height = 21
      BevelOuter = bvNone
      TabOrder = 10
      object EditFont: TEdit
        Left = 0
        Top = 0
        Width = 201
        Height = 21
        ReadOnly = True
        TabOrder = 0
      end
      object ButtonFont: TButton
        Left = 201
        Top = 0
        Width = 19
        Height = 21
        Align = alRight
        Caption = '...'
        TabOrder = 1
        OnClick = ButtonFontClick
      end
    end
    object SpinEditBottomMargin: TSpinEdit
      AlignWithMargins = True
      Left = 121
      Top = 295
      Width = 220
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 11
      Value = 0
    end
    object SpinEditHeight: TSpinEdit
      AlignWithMargins = True
      Left = 121
      Top = 163
      Width = 220
      Height = 22
      MaxValue = 1000
      MinValue = 0
      TabOrder = 12
      Value = 0
    end
    object SpinEditLeft: TSpinEdit
      AlignWithMargins = True
      Left = 121
      Top = 207
      Width = 220
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 13
      Value = 0
    end
    object SpinEditLeftMargin: TSpinEdit
      AlignWithMargins = True
      Left = 121
      Top = 229
      Width = 220
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 14
      Value = 0
    end
    object SpinEditRightMargin: TSpinEdit
      AlignWithMargins = True
      Left = 121
      Top = 251
      Width = 220
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 15
      Value = 0
    end
    object SpinEditTop: TSpinEdit
      AlignWithMargins = True
      Left = 121
      Top = 185
      Width = 220
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 16
      Value = 0
    end
    object SpinEditTopMargin: TSpinEdit
      AlignWithMargins = True
      Left = 121
      Top = 273
      Width = 220
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 17
      Value = 0
    end
    object SpinEditWidth: TSpinEdit
      AlignWithMargins = True
      Left = 121
      Top = 141
      Width = 220
      Height = 22
      MaxValue = 1000
      MinValue = 0
      TabOrder = 18
      Value = 0
    end
  end
end
