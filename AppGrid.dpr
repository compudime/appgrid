program AppGrid;

uses
  Vcl.Forms,
  System.SysUtils,
  uMain in 'uMain.pas' {frmMain},
  uConst in 'uConst.pas',
  uSetup in 'uSetup.pas' {frmSetup},
  uProperty in 'uProperty.pas' {FormProperty};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  if ParamCount > 0 then
  begin
    if ParamStr(1) = PARAM_SETUP then
    begin
       Application.CreateForm(TfrmSetup, frmSetup);
       Application.Run;
    end;
  end
  else begin
    Application.CreateForm(TfrmMain, frmMain);
    Application.Run;
  end;
end.






