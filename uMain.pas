unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, System.IniFiles, uConst,
  System.Actions, Vcl.ActnList, Vcl.buttons, TMS.MQTT.Global, TMS.MQTT.Client,
  Vcl.ImgList, System.UITypes, KeySenderU;

type
  TMyPanel = class(TPanel)
  private
    FFocused : boolean;
    procedure SetFocus(const Value: boolean);
  protected
    procedure Paint; override;
  public
    property Focused : boolean read FFocused write SetFocus;
  end;

  TfrmMain = class(TForm)
    gridPanel: TGridPanel;
    tmrInitialize: TTimer;
    ActionList: TActionList;
    acOpenSettings: TAction;
    FlowPanelGeneral: TPanel;
    MQTTClient: TTMSMQTTClient;
    TimerMQTT: TTimer;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tmrInitializeTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure gridPanelResize(Sender: TObject);
    procedure acOpenSettingsExecute(Sender: TObject);
    procedure TimerMQTTTimer(Sender: TObject);
  private
    { Private declarations }
    FAutoSize: Boolean;
    FShowGeneralBar : Boolean;
    FGeneralBarSize,FGeneralBarAlign: Integer;
    FMQTTServer, FMQTTClientId : string;
    FProgramMatrix: TProgramInfoMatrix;
    FgHeight: Integer;
    FgWidth: Integer;
    FIniFile: TIniFile;
    FTimerScan: TTimer;
    FTimerActive: TTimer;
    FTimerActionButton: TTimer;
    FSelectedPanel: TMyPanel;
    FMQTTList : TList;
//    FSelectedAppPanel: TPanel;
    procedure ExecuteApp(App, Params, Title, ClassName: String; var Handle: HWND; var ErrorCode: Integer);
    procedure GenerateGrid;
    procedure LoadProgramsInfo;
    procedure ExecuteProgram(Col, Row: Integer; var Prog: TProgramInfo; AIgnoreStartOption : boolean = False);
    procedure SetTitlePanel(Col, Row: Integer; Title: String;
      Visible: Boolean; ATitleSize, ATitleColor: Integer;
      ATitleFont : TFont);
    procedure ResizePanel(Panel: TPanel);
    function  FindAppPanel(Col, Row: Integer): TPanel;
    procedure ProgramsScan(Sender: TObject);
    procedure AutoSize;
    procedure btnPlayClick(Sender: TObject);
    procedure btnPauseClick(Sender: TObject);
    procedure btnReloadClick(Sender: TObject);
    procedure btnActionBarClick(Sender: TObject);
    procedure OnPanelClick(Sender : TObject);
    procedure OnTitleClick(Sender : TObject);
    procedure UpdateIdleTimeToPanel(Sender: TObject);
    procedure UpdateTitleFont(APanel : TMyPanel);
    function GetPanelWithHandle(Sender: TObject) : TPanel;
    procedure ClientOnConnectedStatusChanged(ASender: TObject; const AConnected: Boolean; AStatus: TTMSMQTTConnectionStatus);
    procedure ClientOnPacketSent(ASender: TObject; APacketInfo: TTMSMQTTPacketInfo);
    procedure UpdateActiveProgram(Sender : TObject);
    function GetValue4Key(AString : string; AChar : Char; var ACount : Integer): SmallInt;
    procedure TogglePlayAndStopEnable(APanel: TPanel; APlayEnable : boolean);
    procedure SendMQTT(AMQTTData, ATopic : string);
    function IsValid(AButton: TMyButton) : boolean;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;
  MK, FEvent1 : integer;
  OldInputTick: DWORD = 0;
  SecondsCount: Int64 = 0;
  FIdleTime : integer;

implementation

Uses ShellApi, uSetup;

var
  slHandles: TStringList = nil;

{$R *.dfm}

procedure WinEventProc(hWinEventHook: THandle; event: DWORD;
  hwnd: HWND; idObject, idChild: Longint; idEventThread, dwmsEventTime: DWORD); stdcall;
var
  ClassName: string;
  LControl : TWinControl;
  ProcessId : DWORD;
begin
  SetLength(ClassName, 255);
  SetLength(ClassName, GetClassName(hWnd, pchar(ClassName), 255));
    if event = EVENT_OBJECT_FOCUS then
    begin
      if hWnd <> 0 then
      begin
        LControl := FindControl(GetAncestor(hWnd,GA_PARENT));
        if Assigned(LControl) and (LControl is TPanel) then
        begin
          if TPanel(LControl).Parent is TMyPanel then
          begin
            if Assigned(frmMain.FSelectedPanel) then
            begin
              frmMain.FSelectedPanel.Focused := False;
              frmMain.UpdateTitleFont(frmMain.FSelectedPanel);
            end;
            frmMain.FSelectedPanel := TMyPanel(TPanel(LControl).Parent);
            frmMain.FSelectedPanel.Focused := true;
            //frmMain.FSelectedAppPanel := TPanel(LControl);
            frmMain.UpdateTitleFont(frmMain.FSelectedPanel);
          end;
          frmMain.gridPanel.Repaint;
        end;
      end;
    end
    else
    begin
      LControl := FindControl(GetAncestor(hWnd,GA_PARENT));
      if Assigned(LControl) and (LControl is TPanel) then
      begin
        if TPanel(LControl).Parent is TMyPanel then
        begin
          if Assigned(frmMain.FSelectedPanel) then
          begin
            frmMain.FSelectedPanel.Focused := False;
            frmMain.UpdateTitleFont(frmMain.FSelectedPanel);
          end;
        end;
        frmMain.gridPanel.Repaint;
      end;
    end;
end;

function EnumWindowsProc(hwnd: HWND; lParam: LPARAM): BOOL; stdcall;
var
  ProcessId: DWORD;
  EnumData: PEnumData;
begin
  EnumData := PEnumData(lParam);
  GetWindowThreadProcessId(hwnd, ProcessId);
  if EnumData.ProcessID = ProcessID then
  begin
    EnumData.hwnd := hwnd;
    Result := False;
    exit;
  end;
  Result := True;
end;

function LastInputTick: DWORD;
var
  liInfo: TLastInputInfo;
begin
  liInfo.cbSize := SizeOf(TLastInputInfo);
  GetLastInputInfo(liInfo);
  Result := liInfo.dwTime;
end;

function TfrmMain.FindAppPanel(Col, Row: Integer): TPanel;
begin
  Result := TPanel(self.FindComponent('appPanel_'+IntToStr(Col)+'_'+IntToStr(Row)));
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin

  slHandles.Free;
  FTimerActive.Enabled := false;
  FTimerActionButton.Enabled := false;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
   FIniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
   GenerateGrid;
   FSelectedPanel := nil;
//   FSelectedAppPanel:= nil;
   FTimerScan := TTimer.Create(nil);
   FTimerScan.Enabled  := False;
   FTimerScan.Interval := INTERVAL_SCAN * 1000;
   FTimerScan.OnTimer  := ProgramsScan;

   FTimerActive:= TTimer.Create(nil);
   FTimerActive.Enabled  := False;
   FTimerActive.Interval := 1000;
   FTimerActive.OnTimer  := UpdateIdleTimeToPanel;

   FTimerActionButton:= TTimer.Create(nil);
   FTimerActionButton.Enabled  := False;
   FTimerActionButton.Interval := 1000;
   FTimerActionButton.OnTimer  := UpdateActiveProgram;

   FEvent1 := SetWinEventHook(EVENT_OBJECT_FOCUS, EVENT_OBJECT_STATECHANGE, 0, @WinEventProc, 0, 0, WINEVENT_OUTOFCONTEXT);
   FMQTTList := TList.Create;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  FMQTTList.Clear;
  FMQTTList.Free;
  FIniFile.Free;
  FTimerScan.Free;
  FTimerActive.Free;
  UnhookWinEvent(FEvent1);
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  tmrInitialize.Enabled := True;  //load programs first time
end;

procedure TfrmMain.GenerateGrid;
var
  mainPanel : TMyPanel;
  appPanel, otherPanel: TPanel;
  MarginL, MarginR: Integer;
  MarginT, MarginB: Integer;
  iH, iW, LCount, LIndex: Integer;
  sIndex : string;
  cItem: TCellItem;
  LFlowPanel : TPanel;
  LBitButton : TButton;
  LLabel : TLabel;
  LButton : TMyButton;
begin
  //grid section
  FgWidth   := FIniFile.ReadInteger(S_GRID, K_WIDTH, 2);
  FgHeight  := FIniFile.ReadInteger(S_GRID, K_HEIGHT, 2);
  MarginL   := FIniFile.ReadInteger(S_GRID, K_MARGINL, 0);
  MarginR   := FIniFile.ReadInteger(S_GRID, K_MARGINR, 0);
  MarginT   := FIniFile.ReadInteger(S_GRID, K_MARGINT, 0);
  MarginB   := FIniFile.ReadInteger(S_GRID, K_MARGINB, 0);
  FIdleTime := FIniFile.ReadInteger(S_GRID, K_IDLETIME, 60);
  FAutoSize := FIniFile.ReadString(S_GRID, K_AUTOSIZE, 'Y') = 'Y';
  FShowGeneralBar := FIniFile.ReadString(S_GRID, K_GENERAL_SHOW, 'N') = 'Y';
  FGeneralBarAlign := FIniFile.ReadInteger(S_GRID, K_GENERAL_ALIGN , 1);
  FGeneralBarSize := FIniFile.ReadInteger(S_GRID, K_GENERAL_SIZE , 32);
  FMQTTServer := FIniFile.ReadString(S_GRID, 'MQTT_Server', '');
  FMQTTClientId := FIniFile.ReadString(S_GRID, 'MQTT_ClientID', '');
  FlowPanelGeneral.Visible := FShowGeneralBar;
  if FShowGeneralBar then
  begin
    case FGeneralBarAlign of
      0: begin
           FlowPanelGeneral.Align := TAlign.alLeft;
           FlowPanelGeneral.Width := FGeneralBarSize;
         end;
      1: begin
           FlowPanelGeneral.Align := TAlign.alBottom;
           FlowPanelGeneral.Height := FGeneralBarSize;
         end;
      2: begin
           FlowPanelGeneral.Align := TAlign.alRight;
           FlowPanelGeneral.Width := FGeneralBarSize;
         end;
    end;
  end;
  for LIndex := 0 to FlowPanelGeneral.ControlCount - 1 do
  begin
    LButton := TMyButton(FlowPanelGeneral.Controls[LIndex]);
    LButton.Free;
  end;
  LCount := FIniFile.ReadInteger(S_GRID, 'GeneralActionBarButtons',  0);
  for LIndex := 0 to LCount - 1 do
  begin
    LButton := TMyButton.Create(self);
    sIndex := IntToStr(LIndex);
    LButton.Parent := FlowPanelGeneral;
    LButton.Name := 'generalActionBarButton_'+IntToStr(LIndex);
    LButton.Left := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_Left',  0);
    LButton.Top := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_Top',  0);
    LButton.Width := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_Width',  0);
    LButton.Height := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_Height',  0);
    LButton.Margins.Left := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_LeftMargin', Scale(0));
    LButton.Margins.Top  := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_TopMargin', Scale(0));
    LButton.Margins.Right := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_RightMargin',  Scale(0));
    LButton.Margins.Bottom := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_BottomMargin',  Scale(0));

    LButton.Font.Name := FIniFile.ReadString(S_GRID, 'GABButton'+sIndex+'_'+K_FONTNAME, LButton.Font.Name);
    LButton.Font.CharSet := TFontCharSet(FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_'+K_CHARSET, LButton.Font.CharSet));
    LButton.Font.Color := TColor(FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_'+K_FONTCOLOR, LButton.Font.Color));
    LButton.Font.Size := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_'+K_FONTSIZE, LButton.Font.Size);
    LButton.Font.Style := TFontStyles(Byte(FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_'+K_FONTSTYLE, Byte(LButton.Font.Style))));
    LButton.Visible := FIniFile.ReadString(S_GRID, 'GABButton'+sIndex+'_Visible', 'Y') = 'Y';
    LButton.Caption := FIniFile.ReadString(S_GRID, 'GABButton'+sIndex+'_Caption',  '');
    LButton.Gylph := FIniFile.ReadString(S_GRID, 'GABButton'+sIndex+'_Glyph',  '');
    if LButton.Gylph <> '' then
      LButton.OptionsImage.Glyph.LoadFromFile(LButton.Gylph);
    LButton.KeyToSend := FIniFile.ReadString(S_GRID, 'GABButton'+sIndex+'_Key',  '');
    LButton.MQTTdata := FIniFile.ReadString(S_GRID, 'GABButton'+sIndex+'_MQTT',  '');
    LButton.BType := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_BType',  0);
    LButton.BAlign := FIniFile.ReadInteger(S_GRID, 'GABButton'+sIndex+'_Align',  0);
    LButton.OnClick := btnActionBarClick;
  end;


  gridPanel.RowCollection.BeginUpdate;
  gridPanel.ColumnCollection.BeginUpdate;

  //remove posible controls and clear controls
  for iH := 0 to gridPanel.ControlCount-1 do
    GridPanel.Controls[0].Free;
  GridPanel.RowCollection.Clear;
  GridPanel.ColumnCollection.Clear;

  //create row/col to gridPanel
  for iH := 1 to FgHeight do
  begin
    cItem := gridPanel.RowCollection.Add;
    cItem.SizeStyle := ssPercent;
    cItem.Value := 100/FgHeight;
  end;

  for iW := 1 to FgWidth do
  begin
    cItem := gridPanel.ColumnCollection.Add;
    cItem.SizeStyle := ssPercent;
    cItem.Value := 100/FgWidth;
  end;

  //create box panel
  for iH := 0 to FgHeight-1 do
  begin
    for iW := 0 to FgWidth-1 do
    begin
      //main panel for program
      mainPanel := TMyPanel.Create(self);
      mainPanel.Parent := gridPanel;
      mainPanel.Name := 'mainPanel_'+IntToStr(iH)+'_'+IntToStr(iW);
      mainPanel.BevelOuter := bvNone;
      mainPanel.Align := alClient;
      mainPanel.Caption := '';

      //set margins
      otherPanel := TPanel.Create(self);
      otherPanel.Parent := mainPanel;
      otherPanel.Name := 'bPanel_'+IntToStr(iH)+'_'+IntToStr(iW);
      otherPanel.Width := 1;
      if MarginB = 0 then
      begin
        otherPanel.Visible := False;
        otherPanel.Height := 1; //0 is not possible
      end
      else otherPanel.Height := Scale(MarginB);
      otherPanel.BevelOuter := bvNone;
      otherPanel.Align := alBottom;
      otherPanel.Caption := '';

      otherPanel := TPanel.Create(self);
      otherPanel.Parent := mainPanel;
      otherPanel.Name := 'lPanel_'+IntToStr(iH)+'_'+IntToStr(iW);
      otherPanel.Height := 1;
      if MarginL = 0 then
      begin
        otherPanel.Visible := False;
        otherPanel.Width := 1; //0 is not possible
      end
      else otherPanel.Width := Scale(MarginL);
      otherPanel.BevelOuter := bvNone;
      otherPanel.Align := alLeft;
      otherPanel.Caption := '';

      otherPanel := TPanel.Create(self);
      otherPanel.Parent := mainPanel;
      otherPanel.Name := 'rPanel_'+IntToStr(iH)+'_'+IntToStr(iW);
      otherPanel.Height := 1;
      if MarginR = 0 then
      begin
        otherPanel.Visible := False;
        otherPanel.Width := 1; //0 is not possible
      end
      else otherPanel.Width := Scale(MarginR);
      otherPanel.BevelOuter := bvNone;
      otherPanel.Align := alRight;
      otherPanel.Caption := '';

      otherPanel := TPanel.Create(self);
      otherPanel.Parent := mainPanel;
      otherPanel.Name := 'tPanel_'+IntToStr(iH)+'_'+IntToStr(iW);
      otherPanel.Width := 1;
      if MarginT = 0 then
      begin
        otherPanel.Visible := False;
        otherPanel.Height := 1; //0 is not possible
      end
      else otherPanel.Height := Scale(MarginT);
      otherPanel.BevelOuter := bvNone;
      otherPanel.Align := alTop;
      otherPanel.Caption := '';

      //app panel
      appPanel := TPanel.Create(self);
      appPanel.Parent := mainPanel;
      appPanel.Name := 'appPanel_'+IntToStr(iH)+'_'+IntToStr(iW);
      appPanel.Align := alClient;
      appPanel.Caption := '';
      appPanel.Tag := 0;  //here stored the program handle
      appPanel.OnClick := OnPanelClick;

      //title panel
      otherPanel := TPanel.Create(self);
      otherPanel.Parent := appPanel;
      otherPanel.Name := 'titPanel_'+IntToStr(iH)+'_'+IntToStr(iW);
      otherPanel.Height := Scale(TITLE_HEIGHT);
      otherPanel.BevelInner := bvRaised;
      otherPanel.BevelOuter := bvLowered;
      otherPanel.Align := alTop;
      otherPanel.Caption := '';
      otherPanel.ParentBackground := false;
      otherPanel.OnClick := OnTitleClick;

      //Action Bar panel
      LFlowPanel := TPanel.Create(self);
      LFlowPanel.Parent := appPanel;
      LFlowPanel.Name := 'actionBar_'+IntToStr(iH)+'_'+IntToStr(iW);
      LFlowPanel.Align := alBottom;
      LFlowPanel.Caption := '';
      LFlowPanel.Height := Scale(32);
      LFlowPanel.BevelInner := bvRaised;
      LFlowPanel.BevelOuter := bvLowered;
      LFlowPanel.ParentBackground := false;
      LFlowPanel.Tag := 0;  //here stored the program handle
      LFlowPanel.OnClick := OnTitleClick;
    end;
  end;

  gridPanel.RowCollection.EndUpdate;
  gridPanel.ColumnCollection.EndUpdate;
end;

function TfrmMain.GetPanelWithHandle(Sender: TObject): TPanel;
var
  LSender : TObject;
begin
  Result := nil;
  if Sender is TMyButton then
    LSender := TMyButton(Sender).Parent;
  if LSender is TPanel then
    LSender := TPanel(LSender).Parent;
  if LSender is TPanel then
    Result := TPanel(LSender);

end;

function TfrmMain.GetValue4Key(AString: string; AChar : Char; var ACount : Integer): SmallInt;
var
  LFlagCountChange : boolean;
begin
  LFlagCountChange := true;
  if AString.Contains('vkPgDn') then
    Result := vkNext
  else if AString.Contains('vkPgUp') then
    Result := vkPrior
  else if AString.Contains('vkHome') then
    Result := vkHome
  else if AString.Contains('vkEnd') then
    Result := vkEnd
  else if AString.Contains('vkRight') then
    Result := vkRight
  else if AString.Contains('vkLeft') then
    Result := vkLeft
  else if AString.Contains('vkUp') then
    Result := vkUp
  else if AString.Contains('vkDown') then
    Result := vkDown
  else
  begin
    Result := VkKeyScan(AChar);
    LFlagCountChange := false;
  end;
  if LFlagCountChange then
    ACount := ACount - Length(AString);
end;

procedure TfrmMain.UpdateActiveProgram;
var
  ProcessId : DWORD;
  iW, iH : Integer;
  LProgram: TProgramInfo;
  LPanel, LActionBar : TPanel;
  sSection: String;
  LEnablePlay : boolean;
begin
  FTimerScan.Enabled := false;
  for iH := 0 to frmMain.FgHeight-1 do
  begin
    for iW := 0 to frmMain.FgWidth-1 do
    begin
      LEnablePlay := False;
      LProgram := frmMain.FProgramMatrix[iH, iW];
      LPanel := FindAppPanel(iH,iW); //TPanel(LProgram.ParentPanel);
      LActionBar := TPanel(self.FindComponent('actionBar_'+IntToStr(iH)+'_'+IntToStr(iW)));
      if Assigned(LPanel) then
      begin
        ProcessId := 0;
        GetWindowThreadProcessId(LPanel.Tag, ProcessId);
        if ProcessId = 0 then
        begin
          LPanel.Tag := 0;
          sSection := StringReplace(LPanel.Name,'appPanel',S_PROGRAM,[]);
          frmMain.FIniFile.WriteInteger(sSection, 'Active', 0);
        end
        else
        begin
          sSection := StringReplace(LPanel.Name,'appPanel',S_PROGRAM,[]);
          frmMain.FIniFile.WriteInteger(sSection, 'Active', 1);
        end;
      end;
      LEnablePlay := (LPanel = nil) or (LPanel.Tag = 0);
      TogglePlayAndStopEnable(LActionBar, LEnablePlay);
    end;
  end;
end;

procedure TfrmMain.UpdateIdleTimeToPanel(Sender: TObject);
 function ForceForeground(AppHandle:HWND): boolean;
const
 SPI_GETFOREGROUNDLOCKTIMEOUT = $2000;
 SPI_SETFOREGROUNDLOCKTIMEOUT = $2001;
var
 ForegroundThreadID: DWORD;
 ThisThreadID      : DWORD;
 timeout           : DWORD;
 OSVersionInfo     : TOSVersionInfo;
 Win32Platform     : Integer;
begin
 if IsIconic(AppHandle) then ShowWindow(AppHandle, SW_RESTORE);
 if (GetForegroundWindow = AppHandle) then Result := true else
 begin
   Win32Platform := 0;
   OSVersionInfo.dwOSVersionInfoSize := SizeOf(OSVersionInfo);
   if GetVersionEx(OSVersionInfo) then Win32Platform := OSVersionInfo.dwPlatformId;

   if ((Win32Platform = VER_PLATFORM_WIN32_NT) and (OSVersionInfo.dwMajorVersion > 4)) or
      ((Win32Platform = VER_PLATFORM_WIN32_WINDOWS) and ((OSVersionInfo.dwMajorVersion > 4) or
      ((OSVersionInfo.dwMajorVersion = 4) and (OSVersionInfo.dwMinorVersion > 0)))) then
   begin
     Result := false;
     ForegroundThreadID := GetWindowThreadProcessID(GetForegroundWindow,nil);
     ThisThreadID := GetWindowThreadPRocessId(AppHandle,nil);
     if AttachThreadInput(ThisThreadID, ForegroundThreadID, true) then
     begin
       BringWindowToTop(AppHandle);
       SetForegroundWindow(AppHandle);
       AttachThreadInput(ThisThreadID, ForegroundThreadID, false);
       Result := (GetForegroundWindow = AppHandle);
     end;
     if not Result then
     begin
       SystemParametersInfo(SPI_GETFOREGROUNDLOCKTIMEOUT, 0, @timeout, 0);
       SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, TObject(0), SPIF_SENDCHANGE);
       BringWindowToTop(AppHandle);
       SetForegroundWindow(AppHandle);
       SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, TObject(timeout), SPIF_SENDCHANGE);
       Result := (GetForegroundWindow = AppHandle);
       if not Result then
         begin
         ShowWindow(AppHandle,SW_HIDE);
         ShowWindow(AppHandle,SW_SHOWMINIMIZED);
         ShowWindow(AppHandle,SW_SHOWNORMAL);
         BringWindowToTop(AppHandle);
         SetForegroundWindow(AppHandle);
         end;
     end;
   end else
   begin
     BringWindowToTop(AppHandle);
     SetForegroundWindow(AppHandle);
   end;
   Result := (GetForegroundWindow = AppHandle);
 end;
end;

var
  InputTick: DWORD;
  WindowHandle : HWND;
begin
  InputTick := LastInputTick;

  if OldInputTick <> InputTick then
  begin
    OldInputTick := InputTick;
    SecondsCount := 0;
  end
  else
  begin
    SecondsCount := SecondsCount + 1;
  end;

  if SecondsCount >= FIdleTime then
  begin
    if Assigned(frmMain.FSelectedPanel) then
    begin
      frmMain.FSelectedPanel.Focused := False;
      frmMain.UpdateTitleFont(frmMain.FSelectedPanel);
      frmMain.FSelectedPanel := nil;
//      frmMain.FSelectedAppPanel := nil;
      WindowHandle := FindWindow('TfrmMain', nil);
      ForceForeground(WindowHandle);
    end;
    frmMain.gridPanel.Repaint;
  end;
end;

procedure TfrmMain.UpdateTitleFont(APanel: TMyPanel);
var
  LName : string;
  LProg : TProgramInfo;
  LPanel : TPanel;
  LTemp : TStringList;
  LCol, LRow : Integer;
begin
  LName := StringReplace(APanel.Name,'mainPanel','titPanel',[]);
  LTemp := TStringList.Create;
  LTemp.Delimiter := '_';
  LTemp.DelimitedText := APanel.Name;
  if LTemp.Count > 1 then
  begin
    LCol := StrToInt(LTemp[1]);
    LRow := StrToInt(LTemp[2]);
  end;
  LPanel := FindAppPanel(LCol, LRow);
//  if LPanel.Tag <> 0 then
  begin
    LProg := FProgramMatrix[LCol,LRow];
    if not LProg.ShowTitle then
      exit;
    if not Assigned(LProg.TitleFont) then
      exit;
    LPanel := TPanel(self.FindComponent(LName));
    if APanel.Focused then
    begin
      LPanel.Font := LProg.TitleFont;
      LPanel.Color := LProg.TitleColor;
    end
    else
    begin
      LPanel.Font := LProg.UnselectedFont;
      LPanel.Color := LProg.UnselectedTitleColor;
    end;
  end;
end;

procedure TfrmMain.gridPanelResize(Sender: TObject);
var
  iH, iW: Integer;
  panel: TPanel;
begin
  //resize programs
  for iH := 0 to FgHeight-1 do
  begin
    for iW := 0 to FgWidth-1 do
    begin
      panel := FindAppPanel(iH, iW);
      ResizePanel(panel);
    end;
  end;

end;

function TfrmMain.IsValid(AButton: TMyButton): boolean;
var
  LPass : string;
begin
  Result := False;
  if AButton.BPwd <> '' then
  begin
   LPass := InputBox('Password', #31'Please enter password','');
   if LPass <> AButton.BPwd then
   begin
     ShowMessage('Incorrect Password.');
     Exit;
   end
   else
     Result := True;
  end;
  if (AButton.BMsg <> '') then
  begin
    if MessageDlg(AButton.BMsg,TMsgDlgType.mtConfirmation,mbYesNo,0) = mrYes then
      Result := true;
  end;
  if (AButton.BMsg = '') and (AButton.BPwd = '') then
    Result := True;

end;

procedure TfrmMain.LoadProgramsInfo;
var
  iW, iH, LIndex, LCount, LActCount, LMQTTCount: Integer;
  sSection: String;
  pName, pClass, pTitle: String;
  pPath, pParams: String;
  pShowCaption, pShowTitle, pShowIcons: Boolean;
  pProgram: TProgramInfo;
  sPartName, sIndex: String;
  LPanel : TPanel;
  LButton : TMyButton;
  MQTTDetails : TMQTTDetails;
begin
  //read programs info, only those configured will appear
  for iH := 0 to FgHeight-1 do
  begin
    for iW := 0 to FgWidth-1 do
    begin
      sSection := S_PROGRAM+'_'+IntToStr(iH)+'_'+IntToStr(iW);
      if FIniFile.SectionExists(sSection) then
      begin
        pName        := FIniFile.ReadString(sSection, K_NAME, '');
        pClass       := FIniFile.ReadString(sSection, K_CLASS, '');
        pTitle       := FIniFile.ReadString(sSection, K_TITLE, '');
        pPath        := FIniFile.ReadString(sSection, K_APP, '');
        pParams      := FIniFile.ReadString(sSection, K_PARAMS, '');
        pShowCaption := FIniFile.ReadString(sSection, K_SHOWCAPTION, 'Y') = 'Y';
        pShowTitle   := FIniFile.ReadString(sSection, K_SHOWTITLE, 'Y') = 'Y';
        pShowIcons   := FIniFile.ReadString(sSection, K_SHOWICONS, 'Y') = 'Y';

        pProgram.Hwnd   := 0;
        pProgram.Name   := pName;
        pProgram.ClassN := pClass;
        pProgram.Title  := pTitle;
        pProgram.Path   := pPath;
        pProgram.Params := pParams;
        pProgram.ShowCaption := pShowCaption;
        pProgram.ShowTitle   := pShowTitle;
        pProgram.ShowIcons   := pShowIcons;
        pProgram.TitleFont   := TFont.Create;
        pProgram.TitleFont.Name    := FIniFile.ReadString(sSection, K_FONTNAME, Font.Name);
        pProgram.TitleFont.CharSet := TFontCharSet(FIniFile.ReadInteger(sSection, K_CHARSET, Font.CharSet));
        pProgram.TitleFont.Color   := TColor(FIniFile.ReadInteger(sSection, K_FONTCOLOR, Font.Color));
        pProgram.TitleFont.Size    := FIniFile.ReadInteger(sSection, K_FONTSIZE, Font.Size);
        pProgram.TitleFont.Style   := TFontStyles(Byte(FIniFile.ReadInteger(sSection, K_FONTSTYLE, Byte(Font.Style))));

        pProgram.UnselectedFont   := TFont.Create;
        pProgram.UnselectedFont.Name    := FIniFile.ReadString(sSection, K_FONTNAME_UN, Font.Name);
        pProgram.UnselectedFont.CharSet := TFontCharSet(FIniFile.ReadInteger(sSection, K_CHARSET_UN, Font.CharSet));
        pProgram.UnselectedFont.Color   := TColor(FIniFile.ReadInteger(sSection, K_FONTCOLOR_UN, Font.Color));
        pProgram.UnselectedFont.Size    := FIniFile.ReadInteger(sSection, K_FONTSIZE_UN, Font.Size);
        pProgram.UnselectedFont.Style   := TFontStyles(Byte(FIniFile.ReadInteger(sSection, K_FONTSTYLE_UN, Byte(Font.Style))));

        pProgram.TitleSize   := FIniFile.ReadInteger(sSection, 'TitleSize', 20);
        pProgram.TitleColor   := FIniFile.ReadInteger(sSection, 'BackgroundColor', clBtnFace);
        pProgram.UnselectedTitleColor := FIniFile.ReadInteger(sSection, 'UnSelectedColor', clBtnFace);
        SetTitlePanel(iH, iW, pProgram.Title, pProgram.ShowTitle,
          pProgram.TitleSize,pProgram.TitleColor, pProgram.TitleFont);
        pProgram.ShowActionBar := FIniFile.ReadString(sSection, 'ActionBar', 'N') = 'Y';
        pProgram.ShowButtonsInActionBar   := FIniFile.ReadString(sSection, 'ButtonsInActionBar', 'N') = 'Y';
        pProgram.ActionBarSize := FIniFile.ReadInteger(sSection, 'ActionBarSize', 30);
        pProgram.ActionBarAlign := FIniFile.ReadInteger(sSection, 'ActionBarAlign', 1);
        pProgram.StartOption := FIniFile.ReadInteger(sSection, 'StartOption', 0);
        pProgram.Active := FIniFile.ReadInteger(sSection, 'Active', 0);
        pProgram.ParentPanel := nil;
        ExecuteProgram(iH, iW, pProgram);

        //For Buttons in Action Bar
        sPartName := '_'+IntToStr(iH)+'_'+IntToStr(iW);
        LPanel := TPanel(self.FindComponent('actionBar'+sPartName));
        LPanel.Height := Scale(pProgram.ActionBarSize);
        LPanel.Width := Scale(pProgram.ActionBarSize);

        for LIndex := 0 to LPanel.ControlCount - 1 do
        begin
          LButton := TMyButton(LPanel.Controls[LIndex]);
          LButton.Free;
        end;
        LCount := FIniFile.ReadInteger(sSection, 'ActionBarButtons',  0);
        for LIndex := 0 to LCount - 1 do
        begin
          sIndex := IntToStr(LIndex);
          LButton := TMyButton.Create(self);
          LButton.Parent := LPanel;
          LButton.Name := 'actionBarButton_'+sPartName+IntToStr(LIndex);
          LButton.Caption := FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_Caption',  'N');
          LButton.Left := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_Left', Scale(60));
          LButton.Top  := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_Top', Scale(60));
          LButton.Width := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_Width',  Scale(30));
          LButton.Height := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_Height',  Scale(30));
          LButton.Margins.Left := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_LeftMargin', Scale(0));
          LButton.Margins.Top  := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_TopMargin', Scale(0));
          LButton.Margins.Right := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_RightMargin',  Scale(0));
          LButton.Margins.Bottom := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_BottomMargin',  Scale(0));
          LButton.AlignWithMargins := true;
          LButton.Font.Name := FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_'+K_FONTNAME, LPanel.Font.Name);
          LButton.Font.CharSet := TFontCharSet(FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_'+K_CHARSET, LPanel.Font.CharSet));
          LButton.Font.Color   := TColor(FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_'+K_FONTCOLOR, LPanel.Font.Color));
          LButton.Font.Size    := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_'+K_FONTSIZE, LPanel.Font.Size);
          LButton.Font.Style   := TFontStyles(Byte(FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_'+K_FONTSTYLE, Byte(LPanel.Font.Style))));
          LButton.Visible := FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_Visible', 'Y') = 'Y';
          LButton.Gylph :=  FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_Glyph',  '');
          if LButton.Gylph <> '' then
            LButton.OptionsImage.Glyph.LoadFromFile(LButton.Gylph);
          LButton.KeyToSend := FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_Key',  '');
          LButton.MQTTData := FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_MQTT',  '');
          LButton.BType := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_BType',  0);
          LButton.BAlign := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_Align',  0);
          LButton.Topic := FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_Topic',  '');
          LButton.BMsg := FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_Message',  '');
          LButton.BPwd := FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_Pwd',  '');
          LActCount := FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_MQTTCount',  0);

          for LMQTTCount := 0 to LActCount-1 do
          begin
            MQTTDetails := TMQTTDetails.Create('','',0);
            LButton.MQTTInfo.AddObject(IntToStr(LButton.MQTTInfo.Count + 1),TObject(MQTTDetails));
            TMQTTDetails(LButton.MQTTInfo.Objects[LMQTTCount]).MQTTData :=
              FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_MQTTData'+IntToStr(LMQTTCount),  '');
            TMQTTDetails(LButton.MQTTInfo.Objects[LMQTTCount]).Topic :=
              FIniFile.ReadString(sSection, 'ABButton'+sIndex+'_Topic'+IntToStr(LMQTTCount),  '');
            TMQTTDetails(LButton.MQTTInfo.Objects[LMQTTCount]).Delay :=
              FIniFile.ReadInteger(sSection, 'ABButton'+sIndex+'_Delay'+IntToStr(LMQTTCount),  0);
          end;

          if LButton.BType = 1 then
            LButton.OnClick := btnPlayClick
          else if LButton.BType = 2 then
            LButton.OnClick := btnPauseClick
          else if LButton.BType = 3 then
            LButton.OnClick := btnReloadClick
          else
            LButton.OnClick := btnActionBarClick;
        end;

      end
      else begin
        //box without program settings
        pProgram.Hwnd   := 0;
        pProgram.Name   := '';
        pProgram.ClassN := '';
        pProgram.Title  := '';
        pProgram.Path   := '';
        pProgram.Params := '';
        pProgram.ShowCaption := True;
        pProgram.ShowTitle   := True;
        pProgram.ShowIcons   := True;
        pProgram.ShowActionBar := False;
        pProgram.ShowButtonsInActionBar   := False;
        pProgram.ActionBarSize := 30;
        pProgram.ActionBarAlign := 1;
        pProgram.ParentPanel := nil;
        SetTitlePanel(iH, iW, '** no program settigns **', True, 20, clBtnFace,Font);
      end;

      FProgramMatrix[iH, iW] := pProgram;
    end;  //for W
  end; //for H
  FTimerScan.Enabled := False;
end;

procedure TfrmMain.OnPanelClick(Sender: TObject);
begin
  if Sender is TPanel then
  begin
    if TPanel(Sender).Parent is TMyPanel then
    begin
      if Assigned(FSelectedPanel) then
      begin
        FSelectedPanel.Focused := False;
        UpdateTitleFont(FSelectedPanel);
      end;
      FSelectedPanel := TMyPanel(TPanel(Sender).Parent);
      FSelectedPanel.Focused := true;
//      frmMain.FSelectedAppPanel := TPanel(Sender);
      UpdateTitleFont(FSelectedPanel);
    end;
    gridPanel.Repaint;
  end;
end;

procedure TfrmMain.OnTitleClick(Sender: TObject);
begin
  if Sender is TPanel then
  begin
    OnPanelClick(TPanel(Sender).Parent);
    SetForegroundWindow(TPanel(Sender).Tag);
  end;
end;

procedure TfrmMain.ProgramsScan(Sender: TObject);
Var
  C, R: Integer;
  Prog: TProgramInfo;
  Pending: Boolean;

begin
  //scan programs each every INTERVAL_SCAN seconds until complete grid
  FTimerScan.Enabled := False;
  Pending := False;

  //check the matrix with program info to find out which one needs loading
  for C := 0 to FgHeight-1 do
    for R := 0 to FgWidth-1 do
    begin
      Prog := FProgramMatrix[C,R];
      if (Prog.Hwnd = 0) and (FileExists(Prog.Path) or (Prog.Title <> '') or (Prog.ClassN <> '')) then
      begin
        Pending := True;
        ExecuteProgram(C, R, Prog);
      end;
      FProgramMatrix[C,R] := Prog;
    end;

  if FAutoSize then AutoSize;

  FTimerScan.Enabled := Pending;
end;

procedure TfrmMain.ExecuteProgram(Col, Row: Integer; var Prog: TProgramInfo;
  AIgnoreStartOption : boolean = False);
var
  pHwnd: HWND;
  panel, LTitlePanel: TPanel;
  wStyle: NativeInt;
  ErrorCode: Integer;
  LPanelHeight, LPanelWidth, LTitleSize : Integer;
  LFlowPanel : TPanel;
  LHeight, LWidth, LTop, LLeft : Integer;
begin
  pHwnd := 0;
  if FileExists(Prog.Path) or (Prog.Title <> '') or (Prog.ClassN <> '') then
  begin
    if (AIgnoreStartOption) or (Prog.StartOption = 0) or
      ((Prog.StartOption = 2) and (Prog.Active = 1)) then
      ExecuteApp(Prog.Path, Prog.Params, Prog.Title, Prog.ClassN, pHwnd, ErrorCode);
    panel := FindAppPanel(Col, Row);
    LTitlePanel := TPanel(self.FindComponent('titPanel_'+IntToStr(Col)+'_'+IntToStr(Row)));
    TPanel(panel.Parent).Caption := '';
    LFlowPanel :=  TPanel(self.FindComponent('actionBar_'+IntToStr(Col)+'_'+IntToStr(Row)));
    LFlowPanel.Visible := Prog.ShowActionBar;
    if Prog.ShowActionBar then
    begin
      case Prog.ActionBarAlign of
        0: LFlowPanel.Align := TAlign.alLeft;
        1: LFlowPanel.Align := TAlign.alBottom;
        2: LFlowPanel.Align := TAlign.alRight;
      end;
    end;
    TogglePlayAndStopEnable(LFlowPanel,true);
    panel.Tag := pHwnd;
    LTitlePanel.Tag := pHwnd;
    LFlowPanel.Tag := pHwnd;
    if Prog.ShowButtonsInActionBar then
    begin
      if (Prog.ActionBarAlign <> 1) and
        (LFlowPanel.Controls[0].Left <> LFlowPanel.Controls[1].Left) then
      begin
        LLeft := LFlowPanel.Controls[1].Left;
        LTop := LFlowPanel.Controls[1].Top;
        LWidth := LFlowPanel.Controls[1].Width;
        LHeight := LFlowPanel.Controls[1].Height;

        LFlowPanel.Controls[1].Top := LLeft;
        LFlowPanel.Controls[1].Left := LTop;
        LFlowPanel.Controls[1].Height := LWidth;
        LFlowPanel.Controls[1].Width := LHeight;
      end;
    end;

    if pHwnd <> 0 then
    begin
      Prog.Hwnd := pHwnd;
      Prog.ParentPanel := panel;
      Winapi.Windows.SetParent(pHwnd, panel.Handle);
      ResizePanel(panel);

      wStyle := GetWindowLong(Handle, GWL_STYLE);
      if not Prog.ShowCaption then wStyle := wStyle and not WS_CAPTION;
      if not Prog.ShowIcons then wStyle := wStyle and not WS_MINIMIZEBOX and not WS_MAXIMIZEBOX and not WS_SYSMENU;
      Winapi.Windows.SetWindowLong(pHwnd, GWL_STYLE, wStyle);
      LPanelHeight := panel.Height;
      LPanelWidth  := panel.Width;
      LTitleSize   := 0;
      if Prog.ShowTitle then
      begin
//        LPanelHeight := LPanelHeight - TITLE_HEIGHT;
        LTitleSize   := Prog.TitleSize;
        LPanelHeight := LPanelHeight - LTitleSize;
      end;
      if Prog.ShowActionBar then
      begin
        if Prog.ActionBarAlign = 1 then
          LPanelHeight := LPanelHeight - Scale(Prog.ActionBarSize)
        else
          LPanelWidth := LPanelWidth - Scale(Prog.ActionBarSize);
      end;
      Winapi.Windows.SetWindowPos(pHwnd, 0, 0, LTitleSize, LPanelWidth, LPanelHeight, SWP_FRAMECHANGED);
      panel.OnClick(panel);
    end;
  end;
end;

procedure TfrmMain.ResizePanel(Panel: TPanel);
var
  hwnd: Winapi.Windows.HWND;
  Rect: TRect;
begin
  hwnd := Panel.Tag;
  Rect := Panel.ClientRect;
  MoveWindow(hwnd, Rect.Left, Rect.Top, Rect.Right-Rect.Left, Rect.Bottom-Rect.Top, True);
end;


procedure TfrmMain.SendMQTT(AMQTTData, ATopic : string);
var
  LMQTTDetails : TMQTTDetails;
begin
  if (trim(AMQTTData) <> '') and
    (trim(FMQTTServer) <> '') and
    (trim(FMQTTClientId) <> '') then
  begin
    MQTTClient.Disconnect;
    MQTTClient.BrokerHostName := FMQTTServer;
    MQTTClient.ClientId := FMQTTClientId;
    MQTTClient.OnConnectedStatusChanged := ClientOnConnectedStatusChanged;
    MQTTClient.OnPacketSent := ClientOnPacketSent;
    MQTTClient.Connect;
    MQTTClient.LastWillSettings.WillMessage := AMQTTData;
    MQTTClient.LastWillSettings.Topic := ATopic;

    if FMQTTList.Count > 0 then
    begin
      LMQTTDetails := TMQTTDetails(FMQTTList[0]);
      TimerMQTT.Interval := LMQTTDetails.Delay * 1000;
      TimerMQTT.Enabled := True;
    end;

  end;
end;

procedure TfrmMain.SetTitlePanel(Col, Row: Integer; Title: String;
  Visible: Boolean; ATitleSize, ATitleColor: Integer;
  ATitleFont : TFont);
var
  panel: TPanel;
begin
  panel := TPanel(self.FindComponent('titPanel_'+IntToStr(Col)+'_'+IntToStr(Row)));
  if panel <> nil then
  begin
    panel.Visible := Visible;
    panel.Caption := Title;
    panel.Height := ATitleSize;
    panel.Color := ATitleColor;
    panel.Font  := ATitleFont;
  end;
end;

procedure TfrmMain.TimerMQTTTimer(Sender: TObject);
var
  LMQTTDetails : TMQTTDetails;
begin
  TimerMQTT.Enabled := False;
  if FMQTTList.Count > 0 then
  begin
    LMQTTDetails := TMQTTDetails(FMQTTList[0]);
    FMQTTList.Delete(0);
    SendMQTT(LMQTTDetails.MQTTData,LMQTTDetails.Topic);
  end;
end;

procedure TfrmMain.tmrInitializeTimer(Sender: TObject);
begin
  tmrInitialize.Enabled := False;
  LoadProgramsInfo;

  if FAutoSize then
     AutoSize;
  FTimerScan.Enabled := True; //scan programs each INTERVAL_SCAN seconds until complete grid
  FTimerActive.Enabled := True;
  FTimerActionButton.Enabled := True;
end;

procedure TfrmMain.TogglePlayAndStopEnable(APanel: TPanel; APlayEnable : boolean);
var
  LIndex : integer;
  LButton : TMyButton;
begin
  for LIndex := 0 to APanel.ControlCount - 1 do
  begin
    LButton := TMyButton(APanel.Controls[LIndex]);
    if LButton.BType = 1 then
      LButton.Enabled := APlayEnable;
    if LButton.BType = 2 then
      LButton.Enabled := (not APlayEnable);
  end;

end;

procedure TfrmMain.acOpenSettingsExecute(Sender: TObject);
var
  frmSetup: TfrmSetup;
begin
  frmSetup := TfrmSetup.Create(self);
  frmSetup.ShowModal;
end;

procedure TfrmMain.AutoSize;
Var
  C, R: Integer;
  Prog: TProgramInfo;
begin
  //depending on the loaded programs, I adjust the grid on screen hiding cells

//ACA  citem.
  if FAutoSize then
  begin
    //I go through the list of applications and those that are not working or set, I remove them
    for C := 0 to FgHeight-1 do
      for R := 0 to FgWidth-1 do
      begin
        Prog := FProgramMatrix[C,R];
        if (Prog.Hwnd = 0) then
        begin
          //cItem := gridPanel.Items[C,R];
          //cItem.SizeStyle := ssAbsolute;
          //cItem.Value := 0;
        end;
      end;
  end;

  //aca
  {gridPanel.ColumnCollection.Items.
  Item := gridPanel.ControlCollection.ControlItems[0,0];
  Item.ColumnSpan  := 2;
  Item.RowSpan  := 2;}
end;

procedure TfrmMain.btnActionBarClick(Sender: TObject);
var
  LPanel : TPanel;
  LHandle : HWND;
  LButton : TMyButton;
  inputArray: array[0..5] of TInput;
  LKeyChar : Char;
  LKeyValue : SmallInt;
  LFlagKey : Boolean;
  LString : string;
  LCount,LIndex : integer;
  LKeySender: TKeySender;
  LContinue : boolean;
  LMQTTDetails : TMQTTDetails;
begin
  LFlagKey := false;
  LButton := TMyButton(Sender);
  LContinue := IsValid(LButton);
  if not LContinue then
    Exit;
  if trim(LButton.KeyToSend) <> '' then
  begin
    LPanel := GetPanelWithHandle(Sender);
    LString := LButton.KeyToSend;
    if LPanel <> nil then
    begin
      LHandle := LPanel.Tag;
      SetForegroundWindow(LHandle);
      if LHandle <> 0 then
      begin
        LKeySender := TKeySender.Create(self);
        LKeySender.SendOnlyKeys(LString);
        LKeySender.Free;
      end;
    end;
  end;
  if LButton.MQTTInfo.Count > 0 then
  begin
    for LCount := 0 to LButton.MQTTInfo.Count - 1 do
      FMQTTList.Add(LButton.MQTTInfo.Objects[LCount]);
  end;
  SendMQTT(LButton.MQTTData,LButton.Topic);
end;

procedure TfrmMain.btnPauseClick(Sender: TObject);
var
  LPanel : TPanel;
  LButton : TMyButton;
  LHandle : HWND;
  sSection : string;
  LStringList : TStringList;
  LCol, LRow, LCount : integer;
  LContinue : Boolean;
begin
  LButton := TMyButton(Sender);
  LContinue := IsValid(LButton);
  if LContinue then
  begin
    LPanel := GetPanelWithHandle(Sender);
    if LPanel <> nil then
    begin
      LStringList := TStringList.Create;
      LStringList.Clear;
      LStringList.Delimiter       := '_';
      LStringList.StrictDelimiter := True;
      LStringList.DelimitedText   := LPanel.Name;
      LCol := StrToInt(LStringList[1]);
      LRow := StrToInt(LStringList[2]);
      LStringList.Free;
      LHandle := LPanel.Tag;
      PostMessage(LHandle,WM_CLOSE,0,0);
    end;
    if LButton.MQTTInfo.Count > 0 then
    begin
      for LCount := 0 to LButton.MQTTInfo.Count - 1 do
        FMQTTList.Add(LButton.MQTTInfo.Objects[LCount]);
    end;

    SendMQTT(LButton.MQTTData,LButton.Topic);
  end;
end;

procedure TfrmMain.btnPlayClick(Sender: TObject);
var
  LPanel : TPanel;
  LButton : TMyButton;
  LStringList : TStringList;
  LCol, LRow, LCount : integer;
  LProgram: TProgramInfo;
  sSection : string;
  LContinue : Boolean;
begin
  LButton := TMyButton(Sender);
  LContinue := IsValid(LButton);
  if not LContinue then
    Exit;
  LPanel := GetPanelWithHandle(Sender);
  if LPanel.Tag <> 0 then
    Exit;
  sSection := StringReplace(LPanel.Name,'appPanel',S_PROGRAM,[]);
  LStringList := TStringList.Create;
  LStringList.Clear;
  LStringList.Delimiter       := '_';
  LStringList.StrictDelimiter := True;
  LStringList.DelimitedText   := LPanel.Name;
  LCol := StrToInt(LStringList[1]);
  LRow := StrToInt(LStringList[2]);
  LStringList.Free;
  LProgram := FProgramMatrix[LCol,LRow];
  ExecuteProgram(LCol, LRow, LProgram, True);

  if LButton.MQTTInfo.Count > 0 then
  begin
    for LCount := 0 to LButton.MQTTInfo.Count - 1 do
      FMQTTList.Add(LButton.MQTTInfo.Objects[LCount]);
  end;

  SendMQTT(LButton.MQTTData,LButton.Topic);
end;

procedure TfrmMain.btnReloadClick(Sender: TObject);
var
  LPanel : TPanel;
  LHandle : HWND;
  LButton : TMyButton;
  LStringList : TStringList;
  LCol, LRow, LCount : integer;
  LProgram: TProgramInfo;
  sSection : string;
  LContinue : Boolean;
begin
  LButton := TMyButton(Sender);
  LContinue := IsValid(LButton);
  if LContinue then
  begin
    LPanel := GetPanelWithHandle(Sender);
    if LPanel <> nil then
    begin
      LStringList := TStringList.Create;
      LStringList.Clear;
      LStringList.Delimiter       := '_';
      LStringList.StrictDelimiter := True;
      LStringList.DelimitedText   := LPanel.Name;
      LCol := StrToInt(LStringList[1]);
      LRow := StrToInt(LStringList[2]);
      LStringList.Free;
      LHandle := LPanel.Tag;
      PostMessage(LHandle,WM_CLOSE,0,0);
    end;
    sleep(2000);
    LProgram := FProgramMatrix[LCol,LRow];
    ExecuteProgram(LCol, LRow, LProgram, True);

    if LButton.MQTTInfo.Count > 0 then
    begin
      for LCount := 0 to LButton.MQTTInfo.Count - 1 do
        FMQTTList.Add(LButton.MQTTInfo.Objects[LCount]);
    end;

    SendMQTT(LButton.MQTTData,LButton.Topic);

  end;
end;

procedure TfrmMain.ClientOnConnectedStatusChanged(ASender: TObject;
  const AConnected: Boolean; AStatus: TTMSMQTTConnectionStatus);
begin
  if AConnected then
  begin
    MQTTClient.Publish(MQTTClient.LastWillSettings.Topic,MQTTClient.LastWillSettings.WillMessage,qosAtMostOnce, true);
    MQTTClient.LastWillSettings.WillMessage := '';
    MQTTClient.LastWillSettings.Topic := '';
  end;
end;

procedure TfrmMain.ClientOnPacketSent(ASender: TObject;
  APacketInfo: TTMSMQTTPacketInfo);
begin
  if APacketInfo.PacketType = TTMSMQTTPacketType.mtPUBLISH then
  begin
    MQTTClient.OnPacketSent := nil;
    MQTTClient.Disconnect;
  end;
end;

procedure TfrmMain.ExecuteApp(App, Params, Title, ClassName: String; var Handle: HWND; var ErrorCode: Integer);
var
  SInfo: TStartupInfo;
  PInfo: TProcessInformation;
  Re: LongBool;
  EnumData: TEnumData;
  i: Integer;
  CmdLine: String;
begin;
  Handle := 0;
  ErrorCode := 0;
  //I see if the app is created and running
  if (Title <> '') or (ClassName <> '') then
  begin
    //the program is running?
    if (Title = '') and (ClassName <> '') then
      Handle := FindWindow(PWideChar(ClassName), Nil)
    else
      if (Title <> '') and (ClassName = '') then
        Handle := FindWindow(Nil,PChar(Title))
      else
        Handle := FindWindow(PWideChar(ClassName),PChar(Title));
  end;

  //Could not retrieve instance from selected options, I try to create a new one
  if Handle = 0 then
  begin
    if FileExists(App) then
    begin
      FillChar(SInfo, SizeOf(SInfo), #0);
      FillChar(PInfo, SizeOf(PInfo), #0);
      SInfo.cb := SizeOf(SInfo);
      //SInfo.wShowWindow := SW_HIDE;
      //SInfo.dwFlags := STARTF_USESHOWWINDOW;
      CmdLine := App+' '+Params;
      UniqueString(CmdLine);
      Re := CreateProcess(nil, PWideChar(WideString(CmdLine)), nil, nil, False, 0, nil, nil, SInfo, PInfo);
      CloseHandle(PInfo.hThread);

      if Re then
      begin
        WaitForInputIdle(PInfo.hProcess, INFINITE);

        for i := 1 to 50 do
        begin
          Application.ProcessMessages;
          Sleep(10);
        end;

        EnumData.ProcessID := PInfo.dwProcessId;
        EnumData.hwnd := 0;
        EnumWindows(@EnumWindowsProc, LPARAM(@EnumData));
        Handle := EnumData.hwnd;
      end
      else begin
        ErrorCode := GetLastError;
        Handle := 0;
      end;

      CloseHandle(PInfo.hProcess);
    end; //if FileExists(
  end; //if result = 0
end;

{ TMyPanel }
procedure TMyPanel.Paint;
var
  vRect: TRect;
begin
  inherited;
  if not FFocused then
    exit;
  vRect := GetClientRect;

    // Frame panel
  Canvas.Brush.Color := clBlack;
  Canvas.FrameRect(vRect);
  vRect.Width := vRect.Width-Scale(2);
  vRect.Height := vRect.Height-Scale(2);
  vRect.Offset(Scale(1),Scale(1));
  Canvas.FrameRect(vRect);
  vRect.Width := vRect.Width-Scale(2);
  vRect.Height := vRect.Height-Scale(2);
  vRect.Offset(Scale(1),Scale(1));
  Canvas.FrameRect(vRect);
end;

procedure TMyPanel.SetFocus(const Value: boolean);
begin
  FFocused := Value;
end;


end.
