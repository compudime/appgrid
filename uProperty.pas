unit uProperty;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, system.UITypes,
  Vcl.Samples.Spin, uConst;

type
  TFormProperty = class(TForm)
    Panel1: TPanel;
    ButtonDelete: TButton;
    ButtonPrev: TButton;
    ButtonNext: TButton;
    Panel3: TPanel;
    ButtonAddMQTT: TButton;
    ScrollBox: TScrollBox;
    ComboBoxAlign: TComboBox;
    ComboBoxType: TComboBox;
    ComboBoxVisible: TComboBox;
    EditCaption: TEdit;
    EditKeyToSend: TEdit;
    EditMessage: TEdit;
    EditMQTT: TEdit;
    EditPassword: TEdit;
    EditTopic: TEdit;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    LabelTopic: TLabel;
    Panel2: TPanel;
    EditGlyph: TEdit;
    ButtonGlyph: TButton;
    Button1: TButton;
    PanelFont: TPanel;
    EditFont: TEdit;
    ButtonFont: TButton;
    SpinEditBottomMargin: TSpinEdit;
    SpinEditHeight: TSpinEdit;
    SpinEditLeft: TSpinEdit;
    SpinEditLeftMargin: TSpinEdit;
    SpinEditRightMargin: TSpinEdit;
    SpinEditTop: TSpinEdit;
    SpinEditTopMargin: TSpinEdit;
    SpinEditWidth: TSpinEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ButtonFontClick(Sender: TObject);
    procedure EditKeyToSendKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EditKeyToSendKeyPress(Sender: TObject; var Key: Char);
    procedure ButtonGlyphClick(Sender: TObject);
    procedure ButtonDeleteClick(Sender: TObject);
    procedure ButtonNextClick(Sender: TObject);
    procedure ButtonPrevClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ButtonAddMQTTClick(Sender: TObject);
  private
    { Private declarations }
    FFont : TFont;
    FPropertyData : TPropertyData;
    procedure ShowButtonProperties;
    procedure UpdateButtonProperties;
    procedure CreateMoreMQTT(AIndex : integer);
    procedure DestroyMoreMQTT(AIndex : integer);
  public
    ActiveControl : TObject;
    FlagDelete : boolean;
    { Public declarations }
  end;

var
  FormProperty: TFormProperty;

implementation

{$R *.dfm}

procedure TFormProperty.Button1Click(Sender: TObject);
begin
  EditGlyph.text := '';
end;

procedure TFormProperty.ButtonAddMQTTClick(Sender: TObject);
var
  LButton : TMyButton;
  LCount : Integer;
  MQTTDetails : TMQTTDetails;
begin
  LButton := TMyButton(ActiveControl);
  MQTTDetails := TMQTTDetails.Create('','',0);
  LCount := LButton.MQTTInfo.Count + 1;
  LButton.MQTTInfo.AddObject(IntToStr(LCount),TObject(MQTTDetails));
  CreateMoreMQTT(LCount);
end;

procedure TFormProperty.ButtonDeleteClick(Sender: TObject);
begin
  FlagDelete := True;
  Close;
end;

procedure TFormProperty.ButtonFontClick(Sender: TObject);
var
  dlgFont : TFontDialog;
begin
  dlgFont := TFontDialog.Create(Self);
  dlgFont.Font.Name := FFont.Name;
  dlgFont.Font.Size := FFont.Size;
  dlgFont.Font.Color := FFont.Color;
  dlgFont.Font.Style := FFont.Style;
  dlgFont.Font.Charset := FFont.Charset;
  if dlgFont.Execute then
  begin
    FFont.Name     := dlgFont.Font.Name;
    FFont.Size     := dlgFont.Font.Size;
    FFont.Color    := dlgFont.Font.Color;
    FFont.Style    := dlgFont.Font.Style;
    FFont.Charset  := dlgFont.Font.Charset;
    EditFont.Text := FFont.Name+','+FFont.Size.ToString+','+ColorToString(FFont.Color);
  end;
end;

procedure TFormProperty.ButtonGlyphClick(Sender: TObject);
var
  selectedFile: string;
  dlg: TOpenDialog;
begin
  selectedFile := '';
  dlg := TOpenDialog.Create(nil);
  try
    if EditGlyph.Text = '' then
      dlg.InitialDir := 'C:\';
    dlg.Filter := 'Image Files|*.png;*.jpg;*.bmp;*.ico';
    if dlg.Execute(Handle) then
      selectedFile := dlg.FileName;
  finally
    dlg.Free;
  end;

  if selectedFile <> '' then
  begin
    EditGlyph.Text := selectedFile;
  end;
end;

procedure TFormProperty.ButtonNextClick(Sender: TObject);
var
  LParent : TPanel;
  LIndex : integer;
  LButton : TButton;
begin
  UpdateButtonProperties;
  LParent := TPanel(TButton(ActiveControl).Parent);
  for LIndex := 0 to LParent.ControlCount - 1 do
  begin
    LButton := TButton(LParent.Controls[LIndex]);
    if LButton.Name = TButton(ActiveControl).Name then
    begin
      if LIndex = LParent.ControlCount - 1 then
        ActiveControl := LParent.Controls[0]
      else
        ActiveControl := LParent.Controls[LIndex+1];
      break;
    end;
  end;
  ShowButtonProperties;
end;

procedure TFormProperty.ButtonPrevClick(Sender: TObject);
var
  LParent : TPanel;
  LIndex : integer;
  LButton : TButton;
begin
  UpdateButtonProperties;
  LParent := TPanel(TButton(ActiveControl).Parent);
  for LIndex := 0 to LParent.ControlCount - 1 do
  begin
    LButton := TButton(LParent.Controls[LIndex]);
    if LButton.Name = TButton(ActiveControl).Name then
    begin
      if LIndex = 0 then
        ActiveControl := LParent.Controls[LParent.ControlCount - 1]
      else
        ActiveControl := LParent.Controls[LIndex-1];
      break;
    end;
  end;
  ShowButtonProperties;
end;

procedure TFormProperty.CreateMoreMQTT(AIndex: integer);
var
  LLabel : TLabel;
  LEdit : TEdit;
  LSpinEdit : TSpinEdit;
  LLabelTop, LEditTop : Integer;
begin
  LLabelTop := LabelTopic.Top + ((AIndex-1) * Scale(3 * 23));
  LEditTop := EditTopic.Top + ((AIndex-1) * Scale(3 * 23));

  LLabel := TLabel.Create(self);
  LLabel.Parent := ScrollBox;
  LLabel.Name := 'Label_'+IntToStr(AIndex);
  LLabel.Caption := 'MQTT '+IntToStr(AIndex);
  LLabel.Alignment := taCenter;
  LLabel.AutoSize := False;
  LLabel.Height := Scale(13);
  LLabel.Width := Scale(100);
  LLabel.Top := LLabelTop + LabelTopic.Height + Scale(10);
  LLabel.left := Scale(10);
  LLabelTop := LLabel.Top;

  LEdit := TEdit.Create(self);
  LEdit.Parent := ScrollBox;
  LEdit.Name := 'Edit_'+IntToStr(AIndex);
  LEdit.Text := '';
  LEdit.Height := Scale(21);
  LEdit.Width := Scale(221);
  LEdit.Top := LEditTop + EditTopic.Height + Scale(2);
  LEdit.left := Scale(121);
  LEditTop := LEdit.Top;

  LLabel := TLabel.Create(self);
  LLabel.Parent := ScrollBox;
  LLabel.Name := 'LabelT_'+IntToStr(AIndex);
  LLabel.Caption := 'Topic '+IntToStr(AIndex);
  LLabel.Alignment := taCenter;
  LLabel.AutoSize := False;
  LLabel.Height := Scale(13);
  LLabel.Width := Scale(100);
  LLabel.Top := LLabelTop + LabelTopic.Height + Scale(10);
  LLabel.left := Scale(10);
  LLabelTop := LLabel.Top;

  LEdit := TEdit.Create(self);
  LEdit.Parent := ScrollBox;
  LEdit.Name := 'Edit_Topic_'+IntToStr(AIndex);
  LEdit.Text := '';
  LEdit.Height := Scale(21);
  LEdit.Width := Scale(221);
  LEdit.Top := LEditTop + EditTopic.Height + Scale(2);
  LEdit.left := Scale(121);
  LEditTop := LEdit.Top;

  LLabel := TLabel.Create(self);
  LLabel.Parent := ScrollBox;
  LLabel.Name := 'Label_D_'+IntToStr(AIndex);
  LLabel.Caption := 'Delay '+IntToStr(AIndex)+' in secs';
  LLabel.Alignment := taCenter;
  LLabel.AutoSize := False;
  LLabel.Height := Scale(13);
  LLabel.Width := Scale(100);
  LLabel.Top := LLabelTop + LabelTopic.Height + Scale(10);
  LLabel.left := Scale(10);
  LLabelTop := LLabel.Top;

  LSpinEdit := TSpinEdit.Create(self);
  LSpinEdit.Parent := ScrollBox;
  LSpinEdit.Name := 'Spin_'+IntToStr(AIndex);
  LSpinEdit.Top := LEditTop + EditTopic.Height + Scale(2);
  LSpinEdit.Height := Scale(21);
  LSpinEdit.Width := Scale(221);
  LSpinEdit.Left := Scale(121);
end;

procedure TFormProperty.DestroyMoreMQTT(AIndex: integer);
var
  LLabel : TLabel;
  LEdit : TEdit;
  LSpinEdit : TSpinEdit;
begin
  LLabel := TLabel(self.FindComponent('Label_'+IntToStr(AIndex)));
  if Assigned(LLabel) then
    LLabel.Free;
  LEdit := TEdit(self.FindComponent('Edit_'+IntToStr(AIndex)));
  if Assigned(LEdit) then
    LEdit.Free;
  LLabel := TLabel(self.FindComponent('LabelT_'+IntToStr(AIndex)));
  if Assigned(LLabel) then
    LLabel.Free;
  LEdit := TEdit(self.FindComponent('Edit_Topic_'+IntToStr(AIndex)));
  if Assigned(LEdit) then
    LEdit.Free;
  LLabel := TLabel(self.FindComponent('Label_D_'+IntToStr(AIndex)));
  if Assigned(LLabel) then
    LLabel.Free;
  LSpinEdit := TSpinEdit(self.FindComponent('Spin_'+IntToStr(AIndex)));
  if Assigned(LSpinEdit) then
    LSpinEdit.Free;
end;

procedure TFormProperty.EditKeyToSendKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  LText : string;
begin
  if (Ord(Key) = 16) or
    (Ord(Key) = 17) or
    (Ord(Key) = 18) then
    Exit;
  LText := '';
  //if Key = VK_RETURN then
  begin
    if (ssCtrl in Shift) and (ssAlt in Shift) then
      LText := '{CTRL-ALT-'+ Chr(Key) +'}'
    else if ssCtrl in Shift then
      LText := '{CTRL-'+ Chr(Key) +'}'
    else if ssAlt in Shift then
      LText := '{ALT-'+ Chr(Key) +'}'
    else if Key = vkNext  then
      LText := '{PGDN}'
    else if Key = vkPrior  then
      LText := '{PGUP}'
    else if Key = vkEnd  then
      LText := '{END}'
    else if Key = vkHome  then
      LText := '{HOME}'
    else if Key = VK_CANCEL then
      LText := '{BREAK}'
    else if Key = VK_DELETE then
      LText := '{DELETE}'
    else if Key = VK_RETURN then
      LText := '{ENTER}'
    else if Key = VK_ESCAPE then
      LText := '{ESCAPE}'
    else if Key = VK_INSERT then
      LText := '{INSERT}'
    else if Key = VK_PRINT then
      LText := '{PRINTSCREEN}'
    else if Key = VK_SCROLL then
      LText := '{SCROLLLOCK}'
    else if Key = VK_TAB then
      LText := '{TAB}'
    else if Key = VK_F1 then
      LText := '{F1}'
    else if Key = VK_F2 then
      LText := '{F2}'
    else if Key = VK_F3 then
      LText := '{F3}'
    else if Key = VK_F4 then
      LText := '{F4}'
    else if Key = VK_F5 then
      LText := '{F5}'
    else if Key = VK_F6 then
      LText := '{F6}'
    else if Key = VK_F7 then
      LText := '{F7}'
    else if Key = VK_F8 then
      LText := '{F8}'
    else if Key = VK_F9 then
      LText := '{F9}'
    else if Key = VK_F10 then
      LText := '{F10}'
    else if Key = VK_F11 then
      LText := '{F11}'
    else if Key = VK_F12 then
      LText := '{F12}'
    else if Key = VK_F13 then
      LText := '{F13}'
    else if Key = VK_F14 then
      LText := '{F14}'
    else if Key = VK_F15 then
      LText := '{F15}'
    else if Key = VK_F16 then
      LText := '{F16}'
    else if Key = VK_F17 then
      LText := '{F17}'
    else if Key = VK_F18 then
      LText := '{F18}'
    else if Key = VK_F19 then
      LText := '{F19}'
    else if Key = VK_F20 then
      LText := '{F20}'
    else if Key = VK_F21 then
      LText := '{F21}'
    else if Key = VK_F22 then
      LText := '{F22}'
    else if Key = VK_F23 then
      LText := '{F23}'
    else if Key = VK_F24 then
      LText := '{F24}'
    else if Key = VK_NUMPAD0 then
      LText := '{NUMPAD0}'
    else if Key = VK_NUMPAD1 then
      LText := '{NUMPAD1}'
    else if Key = VK_NUMPAD2 then
      LText := '{NUMPAD2}'
    else if Key = VK_NUMPAD3 then
      LText := '{NUMPAD3}'
    else if Key = VK_NUMPAD4 then
      LText := '{NUMPAD4}'
    else if Key = VK_NUMPAD5 then
      LText := '{NUMPAD5}'
    else if Key = VK_NUMPAD6 then
      LText := '{NUMPAD6}'
    else if Key = VK_NUMPAD7 then
      LText := '{NUMPAD7}'
    else if Key = VK_NUMPAD8 then
      LText := '{NUMPAD8}'
    else if Key = VK_NUMPAD9 then
      LText := '{NUMPAD9}'
    else if Key = VK_MULTIPLY then
      LText := '{NUMPADMULTIPLY}'
    else if Key = VK_ADD then
      LText := '{NUMPADADD}'
    else if Key = VK_SUBTRACT then
      LText := '{NUMPADSUBTRACT}'
    else if Key = VK_DECIMAL then
      LText := '{NUMPADDIVIDE}';

    EditKeyToSend.Text := EditKeyToSend.Text + LText;
    if Key = VK_DELETE then
    begin
      Key := 0;
      Exit;
    end;
  end;
end;

procedure TFormProperty.EditKeyToSendKeyPress(Sender: TObject; var Key: Char);
begin
  if Key in [#22, #3] then
    Key := #0;
end;

procedure TFormProperty.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  UpdateButtonProperties;
end;

procedure TFormProperty.FormShow(Sender: TObject);
var
  LText : string;
begin
  LText := TMyButton(ActiveControl).Name;
  ComboBoxType.Items.Clear;
  ComboBoxType.Items.Add('Custom');
  if not LText.Contains('generalActionBarButton_') then
  begin
    ComboBoxType.Items.Add('Play');
    ComboBoxType.Items.Add('Stop');
    ComboBoxType.Items.Add('Reload');
  end;
  ShowButtonProperties;
end;

procedure TFormProperty.ShowButtonProperties;
var
  LButton : TMyButton;
  LFlagEnable : boolean;
  LIndex : integer;
  MQTTDetails : TMQTTDetails;
  LEdit : TEdit;
  LSpinEdit : TSpinEdit;
begin
  FlagDelete := false;
  LButton := TMyButton(ActiveControl);
  LFlagEnable :=  (LButton.Tag = 0);
  ButtonDelete.Enabled := LFlagEnable;
  EditKeyToSend.Enabled := LFlagEnable;
  EditMQTT.Enabled := LFlagEnable;
  EditTopic.Enabled := LFlagEnable;

  FFont := LButton.Font;
  EditCaption.Text := LButton.Caption;
  if LButton.Visible then
    ComboBoxVisible.ItemIndex := 0
  else
    ComboBoxVisible.ItemIndex := 1;
  EditFont.Text := LButton.Font.Name+','+LButton.Font.Size.ToString+','+ColorToString(LButton.Font.Color);
  SpinEditHeight.Value := Scale(LButton.Height, False);
  SpinEditWidth.Value := Scale(LButton.Width, False);
  SpinEditTop.Value := Scale(LButton.Top, False);
  SpinEditLeft.Value := Scale(LButton.Left, False);
  SpinEditLeftMargin.Value := Scale(LButton.Margins.Left, False);
  SpinEditRightMargin.Value := Scale(LButton.Margins.Right, False);
  SpinEditTopMargin.Value := Scale(LButton.Margins.Top, False);
  SpinEditBottomMargin.Value := Scale(LButton.Margins.Bottom, False);
  EditKeyToSend.Text := LButton.KeyToSend;
  EditMQTT.Text := LButton.MQTTData;
  ComboBoxType.ItemIndex := LButton.BType;
  ComboBoxAlign.ItemIndex := LButton.BAlign;
  EditTopic.Text := LButton.Topic;
  EditGlyph.Text := LButton.Gylph;
  EditMessage.Text := LButton.BMsg;
  EditPassword.Text := LButton.BPwd;

  for LIndex := 0 to LButton.MQTTInfo.Count-1 do
  begin
    MQTTDetails := TMQTTDetails(LButton.MQTTInfo.Objects[LIndex]);
    CreateMoreMQTT(LIndex+1);
    LEdit := TEdit(self.FindComponent('Edit_'+LButton.MQTTInfo.Strings[LIndex]));
    LEdit.Text := MQTTDetails.MQTTData;
    LEdit := TEdit(self.FindComponent('Edit_Topic_'+LButton.MQTTInfo.Strings[LIndex]));
    LEdit.Text := MQTTDetails.Topic;
    LSpinEdit := TSpinEdit(self.FindComponent('Spin_'+LButton.MQTTInfo.Strings[LIndex]));
    LSpinEdit.Value := MQTTDetails.Delay;
  end;
end;

procedure TFormProperty.UpdateButtonProperties;
var
  LButton : TMyButton;
  LIndex : integer;
  MQTTDetails : TMQTTDetails;
  LEdit : TEdit;
  LSpinEdit : TSpinEdit;
begin
  LButton := TMyButton(ActiveControl);
  LButton.Caption := EditCaption.Text;
  LButton.Visible := (ComboBoxVisible.ItemIndex = 0);

  LButton.Height := Scale(SpinEditHeight.Value);
  LButton.Width := Scale(SpinEditWidth.Value);
  LButton.Top := Scale(SpinEditTop.Value);
  LButton.Left := Scale(SpinEditLeft.Value);
  LButton.Margins.Left := Scale(SpinEditLeftMargin.Value);
  LButton.Margins.Right := Scale(SpinEditRightMargin.Value);
  LButton.Margins.Top := Scale(SpinEditTopMargin.Value);
  LButton.Margins.Bottom := Scale(SpinEditBottomMargin.Value);
  LButton.AlignWithMargins := true;
  LButton.KeyToSend := EditKeyToSend.Text;
  LButton.MQTTData := EditMQTT.Text;
  LButton.BType := ComboBoxType.ItemIndex;
  LButton.BAlign := ComboBoxAlign.ItemIndex;
  case ComboBoxAlign.ItemIndex of
    0 : LButton.Align := TAlign.alNone;
    1 : LButton.Align := TAlign.alTop;
    2 : LButton.Align := TAlign.alLeft;
    3 : LButton.Align := TAlign.alRight;
    4 : LButton.Align := TAlign.alBottom;
    else
      LButton.Align := TAlign.alNone;
  end;

  LButton.Topic := EditTopic.Text;
  LButton.Gylph := EditGlyph.Text;
  if LButton.Gylph <> '' then
    LButton.OptionsImage.Glyph.LoadFromFile(LButton.Gylph)
  else
  begin
    if Assigned(LButton.OptionsImage.Glyph) then
      LButton.OptionsImage.Glyph.Clear;
  end;
  LButton.Font := FFont;
  LButton.BMsg := EditMessage.Text;
  LButton.BPwd := EditPassword.Text;
  for LIndex := 0 to LButton.MQTTInfo.Count-1 do
  begin
    MQTTDetails := TMQTTDetails(LButton.MQTTInfo.Objects[LIndex]);
    LEdit := TEdit(self.FindComponent('Edit_'+LButton.MQTTInfo.Strings[LIndex]));
    MQTTDetails.MQTTData := LEdit.Text;
    LEdit := TEdit(self.FindComponent('Edit_Topic_'+LButton.MQTTInfo.Strings[LIndex]));
    MQTTDetails.Topic := LEdit.Text;
    LSpinEdit := TSpinEdit(self.FindComponent('Spin_'+LButton.MQTTInfo.Strings[LIndex]));
    MQTTDetails.Delay := LSpinEdit.Value;
    DestroyMoreMQTT(LIndex+1);
  end;
end;

end.
