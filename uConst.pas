unit uConst;

interface

Uses Winapi.Windows, Vcl.Forms, Vcl.Graphics, System.Classes, Vcl.StdCtrls,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, cxButtons;


type
  PEnumData = ^TEnumData;
  TEnumData = record
    ProcessID: DWORD;
    hwnd: HWND;
  end;

  TMQTTDetails = class(TObject)
  private
    FMQTTData : string;
    FTopic : string;
    FDelay : Integer;
  public
    constructor Create(AMQTTData,ATopic : string; ADelay : integer);
    property Topic : String read FTopic write FTopic;
    property MQTTData : String read FMQTTData write FMQTTData;
    property Delay : Integer read FDelay write FDelay;
  end;

  TPropertyData = record
    KeyToSend: string;
    MQTTData: string;
    Glyph : string;
    BType : integer;
    BAlign : integer;
    Topic : string;
    BMsg : string;
    BPwd : string;
  end;

  TMyButton = class(TcxButton)
  private
    FTagObject : TObject;
    FPropertyData : TPropertyData;
    FMQTTInfo : TStringList;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property TagObject : TObject read FTagObject write FTagObject;
    property KeyToSend : String read FPropertyData.KeyToSend write FPropertyData.KeyToSend;
    property MQTTData : String read FPropertyData.MQTTData write FPropertyData.MQTTData;
    property Gylph : String read FPropertyData.Glyph write FPropertyData.Glyph;
    property BType : integer read FPropertyData.BType write FPropertyData.BType;
    property BAlign : integer read FPropertyData.BAlign write FPropertyData.BAlign;
    property Topic : String read FPropertyData.Topic write FPropertyData.Topic;
    property BMsg : String read FPropertyData.BMsg write FPropertyData.BMsg;
    property BPwd : String read FPropertyData.BPwd write FPropertyData.BPwd;
    property MQTTInfo : TStringList read FMQTTInfo write FMQTTInfo;
  end;

  TProgramInfo = Record
    Hwnd: HWND;
    Name: String;
    ClassN: String;
    Title: String;
    ShowCaption: Boolean;
    ShowTitle: Boolean;
    ShowIcons: Boolean;
    Path: String;
    TitleFont: TFont;
    UnselectedFont : TFont;
    Params: String;
	  TitleSize : Integer;
    TitleColor : Integer;
    UnselectedTitleColor : Integer;
    ShowActionBar : Boolean;
    ShowButtonsInActionBar : Boolean;
    ActionBarSize: Integer;
    ActionBarAlign : Integer;
    StartOption : Integer;
    Active : Integer;  //For last state, If program is closed or not
    ParentPanel : TObject;
End;

  TProgramInfoMatrix = Array[0..999, 0..999] of TProgramInfo;


Const

//IniFile Sections and keys-------------------------
  S_LOGIN = 'LOGIN';
  K_PASS = 'PASS';

  S_GRID = 'GRID';
  K_WIDTH = 'WIDTH';
  K_HEIGHT = 'HEIGHT';
  K_MARGINT = 'MARGINT';
  K_MARGINB= 'MARGINB';
  K_MARGINL = 'MARGINL';
  K_MARGINR = 'MARGINR';
  K_IDLETIME = 'IdleTime';
  K_AUTOSIZE = 'AUTOSIZE';
  K_GENERAL_SHOW = 'GENERAL_SHOW';
  K_GENERAL_ALIGN = 'GENERAL_ALIGN';
  K_GENERAL_SIZE = 'GENERAL_SIZE';

  S_PROGRAM = 'PROGRAM'; //this section would add cell number like PROGRAM_0_1 --col 0, row 1
  K_NAME = 'NAME';
  K_CLASS = 'CLASS';
  K_TITLE = 'TITLE';
  K_SHOWCAPTION = 'SHOWCAPTION';
  K_APP = 'APP';
  K_PARAMS = 'PARAMS';
  K_SHOWTITLE = 'SHOWTITLE';
  K_SHOWICONS = 'SHOWICONS';
  K_TITLEFONT = 'TITLEFONT';
  K_FONTNAME = 'FONTNAME';
  K_CHARSET = 'CHARSET';
  K_FONTCOLOR = 'FONTCOLOR';
  K_FONTSIZE = 'FONTSIZE';
  K_FONTSTYLE = 'FONTSTYLE';
  K_FONTNAME_UN = 'UN_FONTNAME';
  K_CHARSET_UN = 'UN_CHARSET';
  K_FONTCOLOR_UN = 'UN_FONTCOLOR';
  K_FONTSIZE_UN = 'UN_FONTSIZE';
  K_FONTSTYLE_UN = 'UN_FONTSTYLE';
  //------------------------------------------------

  TITLE_HEIGHT = 20;
  INTERVAL_SCAN = 60; //in seconds
  PARAM_SETUP = 'setup';
  DEF_PASS = '9999';

  Function Scale(X: Integer; AScale: boolean = true): Integer;

implementation

Function Scale(X: Integer;  AScale: boolean = true): Integer;
begin
  if AScale then
    Result := MulDiv(X, Screen.PixelsPerInch, 96)
  else
    Result := MulDiv(X, 96, Screen.PixelsPerInch);
end;



{ TMyButton }

constructor TMyButton.Create(AOwner: TComponent);
begin
  inherited create(AOwner);
  FMQTTInfo := TStringList.Create;
  FTagObject := nil;
  FPropertyData.KeyToSend := '';
  FPropertyData.MQTTData := '';
  FPropertyData.Glyph := '';
  FPropertyData.BType := 0;
  FPropertyData.BAlign := 0;
  FPropertyData.Topic := '';
end;

destructor TMyButton.Destroy;
begin
  if Assigned(FMQTTInfo) then
  begin
    FMQTTInfo.Clear;
    FMQTTInfo.Free;
    FMQTTInfo := nil;
  end;
  FTagObject := nil;
  FPropertyData.KeyToSend := '';
  FPropertyData.MQTTData := '';
  FPropertyData.Glyph := '';
  FPropertyData.BType := 0;
  FPropertyData.BAlign := 0;
  FPropertyData.Topic := '';
  inherited;
end;

{ TMQTTDetails }

constructor TMQTTDetails.Create(AMQTTData, ATopic: string; ADelay: integer);
begin
  FMQTTData := AMQTTData;
  FTopic := ATopic;
  FDelay := ADelay;
end;

end.
