object frmSetup: TfrmSetup
  Left = 0
  Top = 0
  Caption = 'Setup - AppGrid'
  ClientHeight = 364
  ClientWidth = 1097
  Color = clBtnFace
  Constraints.MinHeight = 370
  Constraints.MinWidth = 900
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    1097
    364)
  PixelsPerInch = 96
  TextHeight = 13
  object gridPanel: TGridPanel
    Left = 0
    Top = 112
    Width = 1097
    Height = 222
    Align = alClient
    BevelOuter = bvNone
    ColumnCollection = <
      item
        Value = 50.000286098475080000
      end
      item
        Value = 49.999713901524920000
      end>
    ControlCollection = <>
    RowCollection = <
      item
        Value = 49.998535141944750000
      end
      item
        Value = 50.001464858055260000
      end>
    TabOrder = 0
    Visible = False
  end
  object pnlGral: TPanel
    Left = 0
    Top = 0
    Width = 1097
    Height = 112
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    object Label2: TLabel
      Left = 648
      Top = 24
      Width = 53
      Height = 13
      Caption = 'Idle in secs'
    end
    object StaticText1: TStaticText
      Left = 0
      Top = 0
      Width = 1097
      Height = 17
      Align = alTop
      Alignment = taCenter
      BevelKind = bkFlat
      Caption = 'General Settings'
      Color = clTeal
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 0
      Transparent = False
    end
    object edPass: TLabeledEdit
      Left = 16
      Top = 40
      Width = 129
      Height = 21
      EditLabel.Width = 46
      EditLabel.Height = 13
      EditLabel.Caption = 'Password'
      LabelSpacing = 2
      PasswordChar = '*'
      TabOrder = 1
    end
    object edGridWidth: TLabeledEdit
      Left = 168
      Top = 40
      Width = 57
      Height = 21
      EditLabel.Width = 50
      EditLabel.Height = 13
      EditLabel.Caption = 'Grid Width'
      LabelSpacing = 2
      MaxLength = 2
      NumbersOnly = True
      TabOrder = 2
    end
    object edGridHeight: TLabeledEdit
      Left = 240
      Top = 40
      Width = 57
      Height = 21
      EditLabel.Width = 53
      EditLabel.Height = 13
      EditLabel.Caption = 'Grid Height'
      LabelSpacing = 2
      MaxLength = 2
      NumbersOnly = True
      TabOrder = 3
    end
    object edMarginTop: TLabeledEdit
      Left = 328
      Top = 40
      Width = 57
      Height = 21
      EditLabel.Width = 53
      EditLabel.Height = 13
      EditLabel.Caption = 'Margin Top'
      LabelSpacing = 2
      MaxLength = 3
      NumbersOnly = True
      TabOrder = 4
    end
    object edMarginBottom: TLabeledEdit
      Left = 408
      Top = 40
      Width = 57
      Height = 21
      EditLabel.Width = 69
      EditLabel.Height = 13
      EditLabel.Caption = 'Margin Bottom'
      LabelSpacing = 2
      MaxLength = 3
      NumbersOnly = True
      TabOrder = 5
    end
    object edMarginLeft: TLabeledEdit
      Left = 488
      Top = 39
      Width = 57
      Height = 21
      EditLabel.Width = 54
      EditLabel.Height = 13
      EditLabel.Caption = 'Margin Left'
      LabelSpacing = 2
      MaxLength = 3
      NumbersOnly = True
      TabOrder = 6
    end
    object edMarginRight: TLabeledEdit
      Left = 568
      Top = 39
      Width = 57
      Height = 21
      EditLabel.Width = 60
      EditLabel.Height = 13
      EditLabel.Caption = 'Margin Right'
      LabelSpacing = 2
      MaxLength = 3
      NumbersOnly = True
      TabOrder = 7
    end
    object ckAutoSize: TCheckBox
      Left = 729
      Top = 40
      Width = 66
      Height = 17
      Caption = 'Auto Size'
      TabOrder = 8
    end
    object StaticText2: TStaticText
      Left = 0
      Top = 95
      Width = 1097
      Height = 17
      Align = alBottom
      Alignment = taCenter
      BevelKind = bkFlat
      Caption = 'Programs Boxes Settings'
      Color = clMaroon
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 9
      Transparent = False
    end
    object btnSave: TButton
      Left = 16
      Top = 67
      Width = 129
      Height = 20
      Caption = 'Apply and Save'
      TabOrder = 10
      OnClick = btnSaveClick
    end
    object btnReload: TButton
      Left = 168
      Top = 67
      Width = 129
      Height = 20
      Caption = 'Reload'
      TabOrder = 11
      OnClick = btnReloadClick
    end
    object Button2: TButton
      Left = 328
      Top = 67
      Width = 129
      Height = 20
      Caption = 'Close'
      TabOrder = 12
      OnClick = Button2Click
    end
    object SpinEditIdle: TSpinEdit
      Left = 648
      Top = 38
      Width = 65
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 13
      Value = 60
    end
    object GroupBox1: TGroupBox
      Left = 801
      Top = 23
      Width = 195
      Height = 50
      Caption = 'General Action Bar'
      TabOrder = 14
      object Label1: TLabel
        Left = 140
        Top = 11
        Width = 19
        Height = 13
        Caption = 'Size'
      end
      object Label3: TLabel
        Left = 62
        Top = 11
        Width = 49
        Height = 13
        Caption = 'Placement'
      end
      object SpinEditGeneralSize: TSpinEdit
        Left = 140
        Top = 26
        Width = 41
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 0
        Value = 60
        OnChange = SpinEditGeneralSizeChange
      end
      object ComboBoxGeneralPlacement: TComboBox
        Left = 61
        Top = 26
        Width = 72
        Height = 21
        Style = csDropDownList
        ItemIndex = 1
        TabOrder = 1
        Text = 'Bottom'
        OnChange = ComboBoxGeneralPlacementChange
        Items.Strings = (
          'Left'
          'Bottom'
          'Right')
      end
      object CheckBoxGeneralShow: TCheckBox
        Left = 2
        Top = 28
        Width = 55
        Height = 17
        Caption = 'Show'
        TabOrder = 2
        OnClick = CheckBoxGeneralShowClick
      end
    end
    object ButtonAddGeneralActionButton: TButton
      Left = 801
      Top = 74
      Width = 195
      Height = 20
      Caption = 'Add General Action Button'
      TabOrder = 15
      OnClick = ButtonAddGeneralActionButtonClick
    end
    object GroupBox2: TGroupBox
      Left = 1002
      Top = 23
      Width = 175
      Height = 66
      Caption = 'MQTT'
      TabOrder = 16
      object Label4: TLabel
        Left = 8
        Top = 16
        Width = 32
        Height = 13
        Caption = 'Server'
      end
      object Label5: TLabel
        Left = 8
        Top = 43
        Width = 41
        Height = 13
        Caption = 'Client ID'
      end
      object EditMQTTServer: TEdit
        Left = 46
        Top = 13
        Width = 126
        Height = 21
        TabOrder = 0
      end
      object EditMQTTClientID: TEdit
        Left = 55
        Top = 39
        Width = 116
        Height = 21
        TabOrder = 1
      end
    end
  end
  object pnlPass: TPanel
    Left = 304
    Top = 192
    Width = 281
    Height = 55
    Anchors = []
    TabOrder = 2
    object edLogin: TLabeledEdit
      Left = 48
      Top = 23
      Width = 193
      Height = 21
      Alignment = taCenter
      EditLabel.Width = 88
      EditLabel.Height = 13
      EditLabel.Caption = 'Input Password'
      EditLabel.Font.Charset = DEFAULT_CHARSET
      EditLabel.Font.Color = clWindowText
      EditLabel.Font.Height = -11
      EditLabel.Font.Name = 'Tahoma'
      EditLabel.Font.Style = [fsBold]
      EditLabel.ParentFont = False
      PasswordChar = '*'
      TabOrder = 0
      OnKeyPress = edLoginKeyPress
    end
  end
  object FlowPanelGeneral: TPanel
    Left = 0
    Top = 334
    Width = 1097
    Height = 30
    Align = alBottom
    TabOrder = 3
  end
  object OpenDialog: TOpenDialog
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Title = 'Select file path'
    Left = 512
    Top = 72
  end
  object TimerShowProperty: TTimer
    Enabled = False
    Interval = 100
    OnTimer = TimerShowPropertyTimer
    Left = 584
    Top = 144
  end
end
